/**
 * @file Generates XML text for entitygroups.xml which adds human NPCs to entity groups used for
 *       biome spawning.
 */

/** The name of the character used as a baseline when calculating difficulty scores. */
const BASELINE_CHARACTER = '__BASELINE__';

/** Whether to print debug information in the output. */
const DEBUG = false;

/** How much to offset the difficutly such that the "midpoint" tier is this many tiers higher */
const DIFFICULTY_TIER_OFFSET = 4;

/**
 * True to divide by the count of weapon varieties, instead of normalizing each character to 1.
 * Ignored if NORMALIZE_CHARACTERS is false.
 */
const DO_COUNT = false;

/** True to normalizing each character at all (by count or to 1). */
const NORMALIZE_CHARACTERS = false;

/** If a weapon is ranged, multiply its score by this amount. */
const RANGED_WEAPON_SCORE_MULTIPLIER = 1.5;

/**
 * The probability of an NPC from this pack to spawn into an entity group,
 * relative to vanilla entity probabilities in that group.
 * 
 * The kind of probabilities it's *relative to* is set by `RELATIVE_TO_INDIVIDUAL`.
 */
// 0.05 = if a user installs 3 packs, the vanilla-to-NPC spawn ratio is roughly 7.5:1
const RELATIVE_PROBABILITY = 0.05;

/**
 * Whether or not `RELATIVE_PROBABILITY` should be the probability of an *individual character*
 * in the pack, relative to the average probability of an *individual vanilla entity*
 * in the entity group.
 * 
 * For example, if there are two vanilla entities in a group, one with a probability of 1,
 * and the other with a probability 2, then `RELATIVE_PROBABILITY` will be relative to a
 * probability of 1.5. If `RELATIVE_PROBABILITY` is 0.1, then the probabilities for
 * *each character in this pack* should add up to 0.15 for this entity group.
 * 
 * If false, then the relative probability should be relative to the *total* probabilities of
 * *all* entities in the group. In the previous example, `RELATIVE_PROBABILITY` will be relative
 * to a probability of 3, and the probabilities for *all the NPCs in the pack* should add up to 0.3.
 */
const RELATIVE_TO_INDIVIDUAL = false;

const factions = buildFactions();
const weapons = buildAllWeapons();
const characters = buildCharacters();
const egs = buildEntityGroupData();

const tiers = egs.length;

buildNpcs(factions, weapons);

calculateAllFactions(factions);

printResults(factions);

function buildCharacters() {
    return [
        // This is a "baseline" character with all weapons and the default health and armor.
        // It is here so that packs which only contain powerful NPCs will be scored appropriately.
        // It will be removed after probabilities are calculated.
        {
            name: BASELINE_CHARACTER,
            health: 300,
            armor: 10,
            weapons: ['EmptyHand', 'Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow'],
            entitygroups: [
                'ZombiesAll',
                'ZombiesForestDowntown',
                'EnemyAnimalsDesert',
                'SnowZombies',
                'ZombiesNight',
                'ZombiesForestDowntownNight',
                'ZombiesDowntown',
                'ZombiesWasteland',
                'ZombiesWastelandDowntown',
                'ZombiesWastelandNight'
            ]
        },
        {
            name: 'FemalePsychoSkinnyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow'],
            entitygroups: [
                'ZombiesBurntForest',
                'EnemyAnimalsDesert',
                'ZombiesWasteland',
                'ZombiesWastelandDowntown',
                'ZombiesWastelandNight'
            ]
        },
        {
            name: 'FemalePsychoStockyKhz',
            health: 400,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow'],
            entitygroups: [
                'ZombiesBurntForest',
                'EnemyAnimalsDesert',
                'ZombiesWasteland',
                'ZombiesWastelandDowntown',
                'ZombiesWastelandNight'
            ]
        },
        {
            name: 'MalePsychoSkinnyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow'],
            entitygroups: [
                'ZombiesBurntForest',
                'EnemyAnimalsDesert',
                'ZombiesWasteland',
                'ZombiesWastelandDowntown',
                'ZombiesWastelandNight'
            ]
        },
        {
            name: 'MalePsychoBruteKhz',
            health: 800,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', /*'Knife',*/ 'Machete', /*'Spear',*/
                'AK47', 'AShotgun', /*'DPistol',*/ /*'HRifle',*/ /*'LBow',*/ 'M60', 'PipeMG', /*'PipePistol',*/
                'PipeRifle', 'PipeShotgun', /*'Pistol',*/ /*'PShotgun',*/ 'RocketL', /*'SMG',*/ /*'SRifle',*/
                'TRifle', 'XBow'],
            entitygroups: [
                'ZombiesBurntForest',
                'EnemyAnimalsDesert',
                'ZombiesWasteland',
                'ZombiesWastelandDowntown',
                'ZombiesWastelandNight'
            ]
        }
    ]
}

function buildFactions() {
    return {
        bandits: {
            All: []
        }
    }
}

function buildEntityGroupData() {
    // This list should be ordered in terms of "difficulty tiers" - biome, downtown, day/night.
    // Halve the totals for night groups, since human NPCs don't like going out at night either.
    return [
        { name: 'ZombiesAll', count: 18, totalProb: 13.7 },
        { name: 'ZombiesForestDowntown', count: 35, totalProb: 17.9 },
        // Halve burnt forest since Rogues also spawn there
        { name: 'ZombiesBurntForest', count: 2, totalProb: 0.125 },
        // Halve desert since Rogues also spawn there
        { name: 'EnemyAnimalsDesert', count: 3, totalProb: 30 },
        { name: 'SnowZombies', count: 1, totalProb: 1 },
        { name: 'ZombiesNight', count: 28, totalProb: 8.65 }, // halved
        { name: 'ZombiesForestDowntownNight', count: 35, totalProb: 10.85 }, // halved
        { name: 'ZombiesDowntown', count: 38, totalProb: 25.57 },
        { name: 'ZombiesWasteland', count: 20, totalProb: 28 },
        { name: 'ZombiesWastelandDowntown', count: 44, totalProb: 38.75 },
        { name: 'ZombiesWastelandNight', count: 21, totalProb: 15 } // halved
    ];
}

function buildNpcs(factions, weapons) {
    factions.bandits.All = weapons.flatMap(weapon => {
        return characters.map(c => {
            return {
                name: `${c.name}${weapon.name}`,
                health: c.health,
                armor: c.armor,
                weapon: weapon,
                probs: [],
                character: c
            };
        });
    }).sort(npcSorter);
}

function buildAllWeapons() {
    return [
        {
            name: 'EmptyHand',
            dmg: 9.1,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Axe',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Bat',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Club',
            dmg: 10,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Machete',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Spear',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'AK47',
            dmg: 13,
            burst: 5,
            isRanged: true
        },
        {
            name: 'AShotgun',
            dmg: 10,
            burst: 4,
            isRanged: true
        },
        {
            name: 'DPistol',
            dmg: 30,
            burst: 4,
            isRanged: true
        },
        {
            name: 'HRifle',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
        {
            name: 'LBow',
            dmg: 30,
            burst: 1,
            isRanged: true
        },
        {
            name: 'M60',
            dmg: 12,
            burst: 10,
            isRanged: true
        },
        {
            name: 'PipeMG',
            dmg: 6,
            burst: 5,
            isRanged: true
        },
        {
            name: 'PipePistol',
            dmg: 12,
            burst: 3,
            isRanged: true
        },
        {
            name: 'PipeRifle',
            dmg: 32,
            burst: 1,
            isRanged: true
        },
        {
            name: 'PipeShotgun',
            dmg: 10,
            burst: 1,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 12,
            burst: 7,
            isRanged: true
        },
        {
            name: 'PShotgun',
            dmg: 10,
            burst: 2,
            isRanged: true
        },
        {
            name: 'RocketL',
            // This is roughly the max damage before it totally throws off the curve
            dmg: 140,
            burst: 1,
            isRanged: true
        },
        {
            name: 'SMG',
            dmg: 10,
            burst: 10,
            isRanged: true
        },
        {
            name: 'SRifle',
            dmg: 35,
            burst: 3,
            isRanged: true
        },
        {
            name: 'TRifle',
            dmg: 13,
            burst: 10,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
    ];
}

function calculateAllFactions(factions) {
    for (let factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            weaponCategoryCalculateAll(factions[factionName]);
        }
    }
}

function calculateProbabilities(npcs, scores) {
    const { maxScore, minScore } = scores;
    const range = maxScore - minScore;

    for (let t = 0; t < tiers; t++) {
        // normalized tier: 0..1 - BUT we count the extra tiers to raise the midpoint
        let tN = t / (tiers - 1 + DIFFICULTY_TIER_OFFSET);
        // tier ratio: -1 to 1.2, min tier to max tier
        // We use 1.2 and not 1 to eliminate the characters with the lowest scores
        // from the highest gamestages
        let tR = 2.2 * tN - 1;
        if (DEBUG) {
            console.log(`<!-- [${t + 1}] ${egs[t].name} ratio: ${tR} -->`);
        }

        for (let i = 0; i < npcs.length; i++) {
            // normalized score, 0..1
            let sN = (npcs[i].score - minScore) / range;
            // a line that rotates around (0.5, 0.5), slope determined by tN;
            // the probability is the y-value on that line when x=sN
            var prob = (tR * sN) - (0.5 * tR) + 0.5;
            // Make sure it's above zero
            prob = Math.max(prob, 0);
            npcs[i].probs[t] = prob;
        }
    }
}

function calculateScores(npcs) {
    let maxScore = 0;
    let minScore = Number.MAX_VALUE;
    let total = 0;

    for (let i = 0; i < npcs.length; i++) {
        let weaponScore = calculateWeaponScore(npcs[i].weapon);

        npcs[i].score =
            (0.25 * npcs[i].health) +
            (2 * npcs[i].armor) +
            weaponScore;

        maxScore = Math.max(maxScore, npcs[i].score);
        minScore = Math.min(minScore, npcs[i].score);
        total += npcs[i].score;
    }

    return {
        maxScore,
        minScore,
        total
    };
}

function calculateTargetProbability(egd) {
    // If we are comparing individual probabilities, then we are trying to match each NPC
    // character's probability to the average vanilla probability, using the relative probability

    let relativeTo = RELATIVE_TO_INDIVIDUAL ?
        egd.totalProb / egd.count :
        egd.totalProb;

    let numCharacters = RELATIVE_TO_INDIVIDUAL ? characters.length : 1;

    return RELATIVE_PROBABILITY * relativeTo * numCharacters;
}

function calculateWeaponScore(weapon) {
    // Use damage over time and give higher scores to ranged
    return weapon.dmg * weapon.burst * (weapon.isRanged ? RANGED_WEAPON_SCORE_MULTIPLIER : 1);
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function normalizeCharacterProbabilities(npcs) {
    if (!NORMALIZE_CHARACTERS) {
        return;
    }

    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be normalized
        for (let tier = 0; tier < tiers; tier++) {
            let divisor = charVariations.length;
            if (!DO_COUNT) {
                divisor = charVariations.reduce(
                    (prev, curr) => prev + curr.probs[tier],
                    0);
            }

            if (divisor <= 1)
                continue;

            charVariations.forEach(cnpc => {
                cnpc.probs[tier] = cnpc.probs[tier] / divisor;
            });
        }
    });
}

function normalizeGamestages(npcs) {
    for (let tier = 0; tier < tiers; tier++) {
        const total = npcs.reduce(
            (prev, curr) => prev + curr.probs[tier],
            0);

        for (const npc of npcs) {
            npc.probs[tier] = npc.probs[tier] / total;
        }
    }
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printFactionXml(faction) {
    for (let t = 0; t < egs.length; t++) {
        let print = false; // Avoid printing if no probabilities are large enough
        let egTotal = 0;
        let targetProb = calculateTargetProbability(egs[t]);
        let xml = [`    <csv xpath="/entitygroups/entitygroup[@name='${egs[t].name}']/text()" delim="\\n" op="add">`];
        faction.All.forEach(npc => {
            if (!npc.character.entitygroups.includes(egs[t].name)) {
                return;
            }
            // Calculate and multiply by the probability relative to the entitygroup target
            let prob = npc.probs[t] * targetProb;
            prob = roundProbability(prob, targetProb);
            if (prob > 0) {
                print = true;
                xml.push(`        ${npc.name}, ${prob}`);
                // For double-checking
                egTotal += prob;
            }
        });
        if (print) {
            // This solves an issue where the last inserted value isn't read,due to the automatic
            // injection of XML comments
            xml.push(`        none, 0`);
            console.log(xml.join('\n'));
            console.log(`    </csv>`);
            if (DEBUG) {
                let individualMessage = RELATIVE_TO_INDIVIDUAL ?
                    ` (${targetProb / characters.length} / character)` :
                    '';
                console.log(`    <!-- Entitygroup total: ${egTotal} / Target total: ${targetProb}${individualMessage} -->\n`);
            }
        }
    }
}

function printResults(factions) {
    console.log(`<configs>`);
    for (let faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            printFactionXml(factions[faction]);
        }
    }
    console.log(`</configs>`);
}

function removeUnusedNpcVariations(npcs) {
    for (let i = 0; i < npcs.length; i++) {
        if (npcs[i].character.name !== BASELINE_CHARACTER &&
            npcs[i].character.weapons.includes(npcs[i].weapon.name))
            continue;
        npcs.splice(i, 1);
        i--;
    }
}

function roundProbability(prob, targetProb) {
    let digits = 100; // 10^2 = 2 digits after the decimal, post-rounding

    while (targetProb > 1) {
        targetProb /= 10;
    }
    while (targetProb < 1) {
        targetProb *= 10;
        digits *= 10;
    }

    return Math.round((prob + Number.EPSILON) * digits) / digits;
}

/**
 * Function to use to calculate probabilities for All weapons.
 *
 * @param {object} faction
 */
function weaponCategoryCalculateAll(faction) {
    const scores = calculateScores(faction.All);
    calculateProbabilities(faction.All, scores);
    removeUnusedNpcVariations(faction.All);

    normalizeCharacterProbabilities(faction.All);

    normalizeGamestages(faction.All);
}
