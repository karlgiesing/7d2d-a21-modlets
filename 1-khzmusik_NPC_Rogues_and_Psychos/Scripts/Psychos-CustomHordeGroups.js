/**
 * @file Generates XML text for entitygroups.xml which adds NPC Pack humans to the custom
 *       entity groups used by sleeper volume spawns.
 *       It does so programatically, so further edits may still be required.
 */

/** True to normalize the probabilities (so they add to 1) */
const DO_NORMALIZATION = false;

/** True to divide by the count of weapon varieties, instead of normalizing */
const DO_COUNT = false;

/**
 * True to scale each character's probabilities (so the minimum is a near-zero number) -
 * this will increase the "spread" of each character's probabilities, without increasing
 * the maximum probability
 */
const DO_SCALING = false;

const BASELINE_CHARACTER = '__BASELINE__';

/** If a weapon is ranged, multiply its score by this amount. */
const RANGED_WEAPON_SCORE_MULTIPLIER = 1.5;

/** Round the probabilities to this number of decimal places. */
const PROB_DECIMALS = 1;

// This is the number of gamestaged entity groups used for wandering hordes.
const gamestages = 50;

const factions = buildFactions();
const weapons = buildAllWeapons();
const characters = buildCharacters();
const gs = buildGameStageStrings();

const tiers = gs.length;

buildNpcs(factions, weapons);

calculateAll(factions);

printResults(factions);

function buildCharacters() {
    return [
        // This is a "baseline" character with all weapons and the default health and armor.
        // It is here so that packs which only contain powerful NPCs will be scored appropriately.
        // It will be removed after probabilities are calculated.
        {
            name: BASELINE_CHARACTER,
            health: 300,
            armor: 10,
            weapons: ['EmptyHand', 'Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemalePsychoSkinnyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemalePsychoStockyKhz',
            health: 400,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MalePsychoSkinnyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MalePsychoBruteKhz',
            health: 800,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', /*'Knife',*/ 'Machete', /*'Spear',*/
                'AK47', 'AShotgun', /*'DPistol',*/ /*'HRifle',*/ /*'LBow',*/ 'M60', 'PipeMG', /*'PipePistol',*/
                'PipeRifle', 'PipeShotgun', /*'Pistol',*/ /*'PShotgun',*/ 'RocketL', /*'SMG',*/ /*'SRifle',*/
                'TRifle', 'XBow']
        }
    ]
}

function buildFactions() {
    return {
        psychos: {}
    }
}

function buildGameStageStrings() {
    // Auto-generated for wandering hordes, which wrap after 50 difficulty stages.
    // This is not very high. So, calculate difficulties for more game stages, but don't use any
    // higher than 50.
    return Array.from(
        { length: gamestages * 3 },
        (v, k) => `wanderingPsychoHordeStageGS${(k + 1).toString().padStart(2, '0')}`
    );
}

function buildNpcs(factions, weapons) {
    factions.psychos = weapons.flatMap(weapon => {
        return characters.map(c => {
            return {
                name: `${c.name}${weapon.name}`,
                health: c.health,
                armor: c.armor,
                weapon: weapon,
                probs: [],
                character: c
            };
        });
    }).sort(npcSorter);
}

function buildAllWeapons() {
    return [
        {
            name: 'EmptyHand',
            dmg: 9.1,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Axe',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Bat',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Club',
            dmg: 10,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Machete',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Spear',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'AK47',
            dmg: 13,
            burst: 5,
            isRanged: true
        },
        {
            name: 'AShotgun',
            dmg: 10,
            burst: 4,
            isRanged: true
        },
        {
            name: 'DPistol',
            dmg: 30,
            burst: 4,
            isRanged: true
        },
        {
            name: 'HRifle',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
        {
            name: 'LBow',
            dmg: 30,
            burst: 1,
            isRanged: true
        },
        {
            name: 'M60',
            dmg: 12,
            burst: 10,
            isRanged: true
        },
        {
            name: 'PipeMG',
            dmg: 6,
            burst: 5,
            isRanged: true
        },
        {
            name: 'PipePistol',
            dmg: 12,
            burst: 3,
            isRanged: true
        },
        {
            name: 'PipeRifle',
            dmg: 32,
            burst: 1,
            isRanged: true
        },
        {
            name: 'PipeShotgun',
            dmg: 10,
            burst: 1,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 12,
            burst: 7,
            isRanged: true
        },
        {
            name: 'PShotgun',
            dmg: 10,
            burst: 2,
            isRanged: true
        },
        {
            name: 'RocketL',
            // This is roughly the max damage before it totally throws off the curve
            // ammoNPCRocketHE's Explosion.EntityDamage is 442; 442 / 3 ~= 147
            dmg: 147,
            burst: 1,
            isRanged: true
        },
        {
            name: 'SMG',
            dmg: 10,
            burst: 10,
            isRanged: true
        },
        {
            name: 'SRifle',
            dmg: 35,
            burst: 3,
            isRanged: true
        },
        {
            name: 'TRifle',
            dmg: 13,
            burst: 10,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
    ];
}

function calculateAll(factions) {
    for (var factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            weaponCategoryCalculateAll(factions[factionName]);
        }
    }
}

function calculateScores(npcs) {
    let maxScore = 0;
    let minScore = Number.MAX_VALUE;
    let total = 0;

    for (var i = 0; i < npcs.length; i++) {
        var weaponScore = calculateWeaponScore(npcs[i].weapon);

        npcs[i].score =
            (0.25 * npcs[i].health) +
            (2 * npcs[i].armor) +
            weaponScore;

        maxScore = Math.max(maxScore, npcs[i].score);
        minScore = Math.min(minScore, npcs[i].score);
        total += npcs[i].score;
    }

    return {
        maxScore,
        minScore,
        total
    };
}

function calculateProbabilities(npcs, scores) {
    const { maxScore, minScore } = scores;
    const range = maxScore - minScore;

    for (let t = 0; t < tiers; t++) {
        // normalized tier: 0..1
        let tN = t / (tiers - 1);
        // tier ratio: -1 to 1.2, min tier to max tier
        // We use 1.2 and not 1 to eliminate the characters with the lowest scores
        // from the highest gamestages
        let tR = 2.2 * tN - 1;

        for (let i = 0; i < npcs.length; i++) {
            // normalized score, 0..1
            let sN = (npcs[i].score - minScore) / range;
            // a line that rotates around (0.5, 0.5), slope determined by tN;
            // the probability is the y-value on that line when x=sN
            var prob = (tR * sN) - (0.5 * tR) + 0.5;
            // Make sure it's in the range of (0,1)
            prob = Math.max(prob, 0);
            prob = Math.min(prob, 1);
            npcs[i].probs[t] = prob;
        }
    }
}

function calculateWeaponScore(weapon) {
    // Use DOT and give higher scores to ranged
    return weapon.dmg * weapon.burst * (weapon.isRanged ? RANGED_WEAPON_SCORE_MULTIPLIER : 1);
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function normalizeProbabilities(npcs) {
    if (!DO_NORMALIZATION && !DO_COUNT)
        return;

    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be normalized
        for (let i = 0; i < tiers; i++) {
            let divisor = charVariations.reduce(
                (prev, curr) => prev + curr.probs[i],
                0);

            if (DO_COUNT) // Divide by character count instead of total score
                divisor = charVariations.length;

            if (divisor <= 1)
                continue;

            charVariations.forEach(cnpc => {
                cnpc.probs[i] = cnpc.probs[i] / divisor;
            });
        }
    });
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printResults(factions) {
    console.log(`<configs>`);
    for (var faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            let factionName = `${faction[0].toUpperCase()}${faction.slice(1)}`;
            printFactionXml(factions[faction], factionName);
        }
    }
    console.log(`\n</configs>`);
}

function printFactionXml(faction, factionName) {
    console.log(`\n    <!-- ${factionName} -->`);

    // for (var weaponType in faction) {
    //     if (faction.hasOwnProperty(weaponType)) {
            // Guard against empty entries
            // if (faction[weaponType].length < 1)
            //     continue;

            for (let t = 0; t < gamestages; t++) {
                console.log(`    <entitygroup name="${gs[t]}">`);
                faction.forEach(npc => {
                    var prob = roundProbability(npc.probs[t]);
                    if (prob >= 0.01) {
                        console.log(`        ${npc.name}, ${prob}`);
                    }
                });
                console.log(`    </entitygroup>`);
            }
    //     }
    // }

    // Handle the "Boss" groups. These should be the same as "All" but one gamestage tier higher.
    // for (let t = 1; t < tiers; t++) {
    //     console.log(`    <entitygroup name="npc${factionName}BossGroup${gs[t - 1]}">`);
    //     faction.forEach(npc => {
    //         var prob = Math.round((npc.probs[t] + Number.EPSILON) * 100) / 100;
    //         if (prob >= 0.01)
    //             console.log(`        <entity name="${npc.name}" prob="${prob}" />`);
    //     });
    //     console.log(`    </entitygroup>`);
    // }
}

function removeUnusedNpcVariations(npcs) {
    for (let i = 0; i < npcs.length; i++) {
        if (npcs[i].character.name !== BASELINE_CHARACTER &&
            npcs[i].character.weapons.includes(npcs[i].weapon.name))
            continue;
        npcs.splice(i, 1);
        i--;
    }
}

function roundProbability(prob, dec = PROB_DECIMALS) {
    // This should accurately round to the number of decimal places
    const mult = Math.pow(10, dec)
    return Math.round((prob + Number.EPSILON) * mult) / mult;
}

function scaleProbabilities(npcs) {
    if (!DO_SCALING)
        return;

    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be scaled
        for (let i = 0; i < tiers; i++) {
            let total = charVariations.reduce(
                (prev, curr) => prev + curr.probs[i],
                0);
            // If the raw total doesn't add up to 1, then do nothing, not even scaling
            // if (total <= 1)
            //     continue;

            // Scale to range [targetMin, max] - the target minimum should be a very low number
            // (so we still scale), and the max shouldn't go higher than the existing max
            // (so if the original total didn't add up to 1, the scaled total won't either)
            let min = Number.MAX_VALUE;
            let max = Number.MIN_VALUE;
            charVariations.forEach(v => {
                min = Math.min(min, v.probs[i]);
                max = Math.max(max, v.probs[i]);
            });

            const targetMin = min / (10 * total);

            charVariations.forEach(cnpc => {
                // If max is the same as min, use the original so we don't divide by zero
                var normalized = max == min ? cnpc.probs[i] : (cnpc.probs[i] - min) / (max - min);
                cnpc.probs[i] = normalized * (max - targetMin) + targetMin;
            });
        }
    });
}

function weaponCategoryCalculateAll(faction) {
    const scores = calculateScores(faction);
    calculateProbabilities(faction, scores);
    removeUnusedNpcVariations(faction);
    // Scale first
    scaleProbabilities(faction);
    normalizeProbabilities(faction);
}
