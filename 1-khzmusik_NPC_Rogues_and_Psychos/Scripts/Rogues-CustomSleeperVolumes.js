/**
 * @file Generates XML text for entitygroups.xml which adds NPC Pack humans to the custom
 *       entity groups used by sleeper volume spawns.
 *       It does so programatically, so further edits may still be required.
 */

/** True to normalize the probabilities (so they add to 1) */
const DO_NORMALIZATION = true;

/** True to divide by the count of weapon varieties, instead of normalizing */
const DO_COUNT = false;

/**
 * True to scale each character's probabilities (so the minimum is a near-zero number) -
 * this will increase the "spread" of each character's probabilities, without increasing
 * the maximum probability.
 */
const DO_SCALING = false;

const BASELINE_CHARACTER = '__BASELINE__';

/** Whether to print debug information in the output. */
const DEBUG = false;

/** The target total probability per entity group */
const TARGET_TOTAL = 10;

/** The score multiplier for ranged weapons. */
const RANGED_SCORE_MULT = 1.5;

const factions = buildFactions();
const weapons = buildAllWeapons();
const characters = buildCharacters();
const gs = buildGameStageStrings();

const tiers = gs.length;

buildNpcs(factions, weapons);

calculateAll(factions);

printResults(factions);

function buildCharacters() {
    return [
        // This is a "baseline" character with all weapons and the default health and armor.
        // It is here so that packs which only contain powerful NPCs will be scored appropriately.
        // It will be removed after probabilities are calculated.
        {
            name: BASELINE_CHARACTER,
            health: 300,
            armor: 10,
            weapons: ['EmptyHand', 'Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueAgedKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueHeavyKhz',
            health: 400,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueLeather01Khz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueLeather02Khz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueAgedKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueBlondeKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueCowboyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueYoungKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        }
    ]
}

function buildFactions() {
    return {
        bandits: {
            All: [],
            Melee: [],
            Ranged: []
        },
        duke: {
            All: [],
            Melee: [],
            Ranged: []
        },
        military: {
            All: [],
            Melee: [],
            Ranged: []
        },
        vault: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whisperers: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whiteriver: {
            All: [],
            Melee: [],
            Ranged: []
        }
    }
}

function buildGameStageStrings() {
    return [
        'GS01',
        'GS50',
        'GS100',
        'GS200',
        'GS400',
        'GS800',
        // Boss gamestage 800
        'GS800'
    ];
}

function buildNpcFaction(factions, factionName, weapons, isFriendly) {
    factions[factionName].All = weapons.flatMap(weapon => {
        return characters.map(c => {
            return {
                name: `${c.name}${weapon.name}`,
                health: c.health,
                armor: c.armor,
                weapon: weapon,
                probs: [],
                character: c
            };
        });
    }).sort(npcSorter);

    factions[factionName].Melee = factions[factionName].All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions[factionName].Ranged = factions[factionName].All.filter(e => e.weapon.isRanged).map(deepClone);

    // if (isFriendly) {
    //     if (!factions.friendly) {
    //         factions.friendly = deepClone(factions[factionName]);
    //     } else {
    //         factions.friendly.All.push(...deepClone(factions[factionName].All));
    //         factions.friendly.Melee.push(...deepClone(factions[factionName].Melee));
    //         factions.friendly.Ranged.push(...deepClone(factions[factionName].Ranged));
    //     }
    // } else {
    //     if (!factions.enemy) {
    //         factions.enemy = deepClone(factions[factionName]);
    //     } else {
    //         factions.enemy.All.push(...deepClone(factions[factionName].All));
    //         factions.enemy.Melee.push(...deepClone(factions[factionName].Melee));
    //         factions.enemy.Ranged.push(...deepClone(factions[factionName].Ranged));
    //     }
    // }

}

function buildNpcs(factions, weapons) {
    buildNpcFaction(factions, 'bandits', weapons, false);
    // buildNpcFaction(factions, 'duke', weapons, true);
    // buildNpcFaction(factions, 'military', weapons, true);
    // buildNpcFaction(factions, 'vault', weapons, true);
    // buildNpcFaction(factions, 'whisperers', weapons, false);
    // buildNpcFaction(factions, 'whiteriver', weapons, true);
}

function buildAllWeapons() {
    return [
        {
            name: 'EmptyHand',
            dmg: 9.1,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Axe',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Bat',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Club',
            dmg: 10,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Machete',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Spear',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'AK47',
            dmg: 13,
            burst: 5,
            isRanged: true
        },
        {
            name: 'AShotgun',
            dmg: 10,
            burst: 4,
            isRanged: true
        },
        {
            name: 'DPistol',
            dmg: 30,
            burst: 4,
            isRanged: true
        },
        {
            name: 'HRifle',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
        {
            name: 'LBow',
            dmg: 30,
            burst: 1,
            isRanged: true
        },
        {
            name: 'M60',
            dmg: 12,
            burst: 10,
            isRanged: true
        },
        {
            name: 'PipeMG',
            dmg: 6,
            burst: 5,
            isRanged: true
        },
        {
            name: 'PipePistol',
            dmg: 12,
            burst: 3,
            isRanged: true
        },
        {
            name: 'PipeRifle',
            dmg: 32,
            burst: 1,
            isRanged: true
        },
        {
            name: 'PipeShotgun',
            dmg: 10,
            burst: 1,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 12,
            burst: 7,
            isRanged: true
        },
        {
            name: 'PShotgun',
            dmg: 10,
            burst: 2,
            isRanged: true
        },
        {
            name: 'RocketL',
            // This is roughly the max damage before it totally throws off the curve
            // ammoNPCRocketHE's Explosion.EntityDamage is 442; 442 / 3 ~= 147
            dmg: 147,
            burst: 1,
            isRanged: true
        },
        {
            name: 'SMG',
            dmg: 10,
            burst: 10,
            isRanged: true
        },
        {
            name: 'SRifle',
            dmg: 35,
            burst: 3,
            isRanged: true
        },
        {
            name: 'TRifle',
            dmg: 13,
            burst: 10,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
    ];
}

function calculateAll(factions) {
    for (var factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            // Choose one according to the benefits you prioritize.
            // weaponCategoryCalculateIndependently(factions[factionName]);
            // weaponCategoryCalculateRangedMeleeCombineAll(factions[factionName]);
            weaponCategoryCalculateAllSplitRangedMelee(factions[factionName]);
        }
    }
}

function calculateScores(npcs) {
    let maxScore = 0;
    let minScore = Number.MAX_VALUE;
    let total = 0;

    // For debugging
    const weaponScores = [];

    for (var i = 0; i < npcs.length; i++) {
        var weaponScore = calculateWeaponScore(npcs[i].weapon);
        weaponScores.push(weaponScore);

        npcs[i].score =
            (0.25 * npcs[i].health) +
            (2 * npcs[i].armor) +
            weaponScore;

        maxScore = Math.max(maxScore, npcs[i].score);
        minScore = Math.min(minScore, npcs[i].score);
        total += npcs[i].score;
    }

    if (DEBUG) {
        console.log(`<!-- Weapon scores: [${weaponScores.join(',')}] -->`);
    }

    return {
        maxScore,
        minScore,
        total
    };
}

function calculateProbabilities(npcs, scores) {
    const { maxScore, minScore } = scores;
    const range = maxScore - minScore;

    for (let t = 0; t < tiers; t++) {
        // normalized tier: 0..1
        let tN = t / (tiers - 1);
        // tier ratio: -1.25 to 1.25, min tier to max tier
        // We use these numbers to eliminate the characters with the lowest scores
        // from the highest gamestages, and vice versa
        let tR = 2.5 * tN - 1.25;

        for (let i = 0; i < npcs.length; i++) {
            // normalized score, 0..1
            let sN = (npcs[i].score - minScore) / range;
            // a line that rotates around (0.5, 0.5), slope determined by tN;
            // the probability is the y-value on that line when x=sN
            var prob = (tR * sN) - (0.5 * tR) + 0.5;
            // Make sure it's in the range of (0,1)
            prob = Math.max(prob, 0);
            prob = Math.min(prob, 1);
            npcs[i].probs[t] = prob;
        }
    }
}

function calculateWeaponScore(weapon) {
    // Use DOT and give higher scores to ranged
    return weapon.dmg * weapon.burst * (weapon.isRanged ? RANGED_SCORE_MULT : 1);
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function normalizeCharacterProbabilities(npcs) {
    if (!DO_NORMALIZATION && !DO_COUNT)
        return;

    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be normalized
        for (let i = 0; i < tiers; i++) {
            let divisor = charVariations.reduce(
                (prev, curr) => prev + curr.probs[i],
                0);

            if (DO_COUNT) // Divide by character count instead of total score
                divisor = charVariations.length;

            if (divisor <= 1)
                continue;

            charVariations.forEach(cnpc => {
                cnpc.probs[i] = cnpc.probs[i] / divisor;
            });
        }
    });
}

function normalizeGamestages(npcs) {
    for (let tier = 0; tier < tiers; tier++) {
        const total = npcs.reduce(
            (prev, curr) => prev + curr.probs[tier],
            0);

        for (const npc of npcs) {
            npc.probs[tier] = npc.probs[tier] / total;
        }
    }
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printResults(factions) {
    console.log(`<configs>`);
    for (var faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            let factionName = `${faction[0].toUpperCase()}${faction.slice(1)}`;
            printFactionXml(factions[faction], factionName);
        }
    }
    console.log(`\n</configs>`);
}

function printFactionXml(faction, factionName) {
    console.log(`\n    <!-- ${factionName} -->`);

    for (var weaponType in faction) {
        if (faction.hasOwnProperty(weaponType)) {
            // Guard against empty entries
            if (faction[weaponType].length < 1)
                continue;

            // Don't print the highest tier, it's boss only
            for (let t = 0; t < tiers - 1; t++) {
                let egTotal = 0;
                console.log(`    <entitygroup name="npcRogues${weaponType}Group${gs[t]}">`);
                faction[weaponType].forEach(npc => {
                    var prob = roundProbability(npc.probs[t] * TARGET_TOTAL);
                    if (prob > 0) {
                        console.log(`        ${npc.name}, ${prob}`);
                        // For double-checking
                        egTotal += prob;
                    } else if (DEBUG) {
                        console.log(`        <!-- ${npc.name}, ${prob} -->`);
                    }
                });
                console.log(`    </entitygroup>`);
                if (DEBUG) {
                    console.log(`    <!-- Entitygroup total: ${egTotal} -->\n`);
                }
            }
        }
    }

    // Handle the "Boss" groups. These should be the same as "All" but one gamestage tier higher.
    for (let t = 1; t < tiers; t++) {
        // Guard against empty entries
        if (faction.All.length < 1)
            continue;

        let egTotal = 0;
        console.log(`    <entitygroup name="npcRoguesBossGroup${gs[t - 1]}">`);
        faction.All.forEach(npc => {
            var prob = roundProbability(npc.probs[t] * TARGET_TOTAL);
            if (prob > 0) {
                console.log(`        ${npc.name}, ${prob}`);
                // For double-checking
                egTotal += prob;
            } else if (DEBUG) {
                console.log(`        <!-- ${npc.name}, ${prob} -->`);
            }
        });
        console.log(`    </entitygroup>`);
        if (DEBUG) {
            console.log(`    <!-- Entitygroup total: ${egTotal} -->\n`);
        }
    }
}

function removeUnusedNpcVariations(npcs) {
    for (let i = 0; i < npcs.length; i++) {
        if (npcs[i].character.name !== BASELINE_CHARACTER &&
            npcs[i].character.weapons.includes(npcs[i].weapon.name))
            continue;
        npcs.splice(i, 1);
        i--;
    }
}

function roundProbability(prob, decimals = 2) {
    // This should accurately round to the given decimal place
    let digits = Math.pow(10, decimals);
    return Math.round((prob + Number.EPSILON) * digits) / digits;
}

function scaleProbabilities(npcs) {
    if (!DO_SCALING)
        return;

    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be scaled
        for (let i = 0; i < tiers; i++) {
            let total = charVariations.reduce(
                (prev, curr) => prev + curr.probs[i],
                0);
            // If the raw total doesn't add up to 1, then do nothing, not even scaling
            // if (total <= 1)
            //     continue;

            // Scale to range [targetMin, max] - the target minimum should be a very low number
            // (so we still scale), and the max shouldn't go higher than the existing max
            // (so if the original total didn't add up to 1, the scaled total won't either)
            let min = Number.MAX_VALUE;
            let max = Number.MIN_VALUE;
            charVariations.forEach(v => {
                min = Math.min(min, v.probs[i]);
                max = Math.max(max, v.probs[i]);
            });

            const targetMin = min / (10 * total);

            charVariations.forEach(cnpc => {
                // If max is the same as min, use the original so we don't divide by zero
                var normalized = max == min ? cnpc.probs[i] : (cnpc.probs[i] - min) / (max - min);
                cnpc.probs[i] = normalized * (max - targetMin) + targetMin;
            });
        }
    });
}

/**
 * Function to use to calculate probabilities for All, then split that into Ranged and Melee.
 * 
 * Benefits:
 * * "All" has a greater variety of difficulty overall - less likely to encounter any kind of
 *   ranged NPC at GS1 than at GS800, and vice versa for melee.
 * * Consistency in weapon spawning between gamestage groups for all, ranged, and melee.
 * 
 * Drawbacks:
 * * "All" has uneven mix of melee and ranged.
 * * "Melee" groups have fewer variations in weapon probabilities.
 *   All first:   at GS001, axe has probability of 0.20 and club has probability of 0.21;
 *                at GS800, axe has probability of 0.21 and club has probability of 0.17.
 *   Independent: at GS001, axe has probability of zero and club has probability of 0.67;
 *                at GS800, axe has probability of 0.26 and club has probability of 0.07.
 * 
 * @param {object} faction
 */
function weaponCategoryCalculateAllSplitRangedMelee(faction) {
    const scores = calculateScores(faction.All);
    calculateProbabilities(faction.All, scores);
    removeUnusedNpcVariations(faction.All);

    // Scale first
    scaleProbabilities(faction.All);

    faction.Melee = faction.All.filter(e => !e.weapon.isRanged).map(deepClone);
    faction.Ranged = faction.All.filter(e => e.weapon.isRanged).map(deepClone);

    for (var weaponCategory in faction) {
        if (faction.hasOwnProperty(weaponCategory)) {
            normalizeCharacterProbabilities(faction[weaponCategory]);
            normalizeGamestages(faction[weaponCategory]);
        }
    }
}

/**
 * Function to use to calculate probabilities for All, Ranged, and Melee independently.
 * 
 * Benefits:
 * * "All" has a greater variation in overall difficulty between gamestages - less likely to
 *   encounter any kind of ranged NPC at GS1 than at GS800, and vice versa for melee.
 * * "Melee" groups have greater variations in weapon probabilities.
 * 
 * Drawbacks:
 * * "All" has uneven mix of melee and ranged depending upon gamestage.
 * * No consistency in weapon spawning between gamestage groups for all, ranged, and melee.
 *   A player can encounter an NPC with a machete at GS1 in "All" but not in "Melee".
 * 
 * @param {object} faction
 */
function weaponCategoryCalculateIndependently(faction) {
    for (var weaponCategory in faction) {
        if (faction.hasOwnProperty(weaponCategory)) {
            const scores = calculateScores(faction[weaponCategory]);
            calculateProbabilities(faction[weaponCategory], scores);
            removeUnusedNpcVariations(faction[weaponCategory]);
            scaleProbabilities(faction[weaponCategory]);
            normalizeCharacterProbabilities(faction[weaponCategory]);
        }
    }
}

/**
 * Function to use to calculate probabilities for Ranged and Melee independently,
 * then combine them into All.
 * 
 * Benefits:
 * * "All" has an even mix of ranged and melee at all gamestages.
 * * Consistency in weapon spawning between gamestage groups for all, ranged, and melee.
 * * "Melee" groups have greater variations in weapon probabilities.
 * 
 * Drawbacks:
 * * "All" has a far less variation in overall difficulty between gamestages - just as likely to
 *   encounter some kind of ranged NPC at GS1 as GS800, and the same for melee.
 *   Example: at GS800, equal probability to encounter an NPC with an axe, as an NPC with an M60.
 * 
 * @param {object} faction
 */
function weaponCategoryCalculateRangedMeleeCombineAll(faction) {
    const scoresMelee = calculateScores(faction.Melee);
    calculateProbabilities(faction.Melee, scoresMelee);
    removeUnusedNpcVariations(faction.Melee);
    scaleProbabilities(faction.Melee);

    const scoresRanged = calculateScores(faction.Ranged);
    calculateProbabilities(faction.Ranged, scoresRanged);
    removeUnusedNpcVariations(faction.Ranged);
    scaleProbabilities(faction.Ranged);

    faction.All = deepClone(faction.Melee)
        .concat(deepClone(faction.Ranged))
        .sort(npcSorter);

    for (var weaponCategory in faction) {
        if (faction.hasOwnProperty(weaponCategory)) {
            normalizeCharacterProbabilities(faction[weaponCategory]);
        }
    }
}
