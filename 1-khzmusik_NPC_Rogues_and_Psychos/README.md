# Rogues and Psychos

Adds "Rogue" and "Psycho" human NPCs to the game.
Both rogues and psychos are part of the Bandits faction.

All characters were created by khzmusik, using assets that are either free for re-use or CC-BY.
See the Technical Details section for more information and asset credits.

## Features

* Eight Rougue characters and four Psycho characters
* Custom AI (crouching between attacks, thrashing around when on fire, etc.)
* "Basic" versions (enabled by default) or "advanced" versions (not enabled by default)
* Rogue camps and Psycho remnant POIs used as biome decorations
  _(New for A21)_
* _For prefab creators:_ Rogue and Psycho sleeper volume spawn groups
  (separate from the NPC Core Bandit groups)
* _For modders:_ Custom wandering horde groups (not enabled by default)
* _For modders:_ XML to make "advanced" Rogue characters part of the non-enemy Whiteriver faction
  (not enabled by default)
* If using "advanced" Rogues or Psychos (enemy or otherwise),
  custom dialog with backstories and personal histories

## Rogues

The pack contains these "Rogue" characters:

---

### Female Rogue Aged
![Female Rogue Aged](./UIAtlases/UIAtlas/npcFemaleRogueAgedKhz.png)

---

### Female Rogue Heavy
![Female Rogue Heavy](./UIAtlases/UIAtlas/npcFemaleRogueHeavyKhz.png)

---

### Female Rogue Leather 1
![Female Rogue Leather 1](./UIAtlases/UIAtlas/npcFemaleRogueLeather01Khz.png)

---

### Female Rogue Leather 2
![Female Rogue Leather 2](./UIAtlases/UIAtlas/npcFemaleRogueLeather02Khz.png)

---

<!-- WAS: MaleRogueJoe -->
### Male Rogue Aged
![Male Rogue Aged](./UIAtlases/UIAtlas/npcMaleRogueAgedKhz.png)

---

### Male Rogue Blonde
![Male Rogue Blonde](./UIAtlases/UIAtlas/npcMaleRogueBlondeKhz.png)

---

### Male Rogue Cowboy
![Male Rogue Cowboy](./UIAtlases/UIAtlas/npcMaleRogueCowboyKhz.png)

---

<!-- WAS: MaleRogueGareth -->
### Male Rogue Young
![Male Rogue Young](./UIAtlases/UIAtlas/npcMaleRogueYoungKhz.png)

---

Every rogue can wield all supported NPC weapons.

## Psychos

The pack contains these "Psycho" characters:

### Female Psycho Skinny
![Female Psycho Skinny](./UIAtlases/UIAtlas/npcFemalePsychoSkinnyKhz.png)

---

### Female Psycho Stocky
![Female Psycho Stocky](./UIAtlases/UIAtlas/npcFemalePsychoStockyKhz.png)

---

### Male Psycho Skinny
![Male Psycho Skinny](./UIAtlases/UIAtlas/npcMalePsychoSkinnyKhz.png)

---

### Male Psycho Brute
![Male Psycho Brute](./UIAtlases/UIAtlas/npcMalePsychoBruteKhz.png)

---

_Most_ psychos wield all supported NPC weapons.

The exception is the **Male Psycho Brute.**
He does _not_ wield:

* Wooden bow
* Pistol
* Desert Eagle
* Knife
* Pipe Pistol
* Spear
* Hunting Rifle
* Pump Shotgun
* SMG
* Sniper Rifle

This is due to his size and posture,
which make him incompatible with the animations for those weapons.

## Decoration Prefabs
_(New for A21)_

This pack contains prefabs that are used as biome decorations.
They spawn _only_ Rogues, or _only_ Psychos.

Because they are biome decorations, they spawn when the chunk is generated, and not during RWG.
This means they can spawn into pre-generated maps, such as Navezgane.

I have tried my best to make it a rare event to encounter one of these prefabs.
Players will probably encounter only one or two of each, depending upon biome.

If you want to change their spawn probabilities,
you can edit this mod's `biomes.xml` file.
If you want to prevent them from spawning altogether,
that entire file can be commented out or just deleted.

---

### Rogue Camp 1
![Rogue Camp 1](./Prefabs/deco_rogue_camp_1.jpg)

Spawns in the Desert biome.

---

### Rogue Camp 2
![Rogue Camp 2](./Prefabs/deco_rogue_camp_2.jpg)

Spawns in the Desert biome.

---

### Rogue Camp 3
![Rogue Camp 3](./Prefabs/deco_rogue_camp_3.jpg)

Spawns in the Pine Forest and Desert biomes.

---

### Rogue Camp 4
![Rogue Camp 4](./Prefabs/deco_rogue_camp_4.jpg)

Spawns in the Snow biome.

---

### Rogue Camp 5
![Rogue Camp 5](./Prefabs/deco_rogue_camp_5.jpg)

Spawns in the Snow biome.

---

### Rogue Camp 6
![Rogue Camp 6](./Prefabs/deco_rogue_camp_6.jpg)

Spawns in the Pine Forest biome.

---

### Psycho Camp 1
![Psycho Camp 1](./Prefabs/deco_psycho_camp_1.jpg)

Spawns in the Desert biome.

---

### Psycho Remnant 1
![Psycho Remnant 1](./Prefabs/deco_remnant_psycho_1.jpg)

Spawns in the Wasteland biome.

---

### Psycho Remnant 2
![Psycho Remnant 2](./Prefabs/deco_remnant_psycho_2.jpg)

Spawns in the Wasteland biome.

---

### Psycho Remnant 3
![Psycho Remnant 3](./Prefabs/deco_remnant_psycho_3.jpg)

Spawns in the Wasteland biome.

---

### Psycho Remnant 4
![Psycho Remnant 4](./Prefabs/deco_remnant_psycho_4.jpg)

Spawns in the Wasteland biome.

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

**Requires SCore version `21.1.24.928` or higher.**
The custom "pick up" items require version `21.2.31.1132` or higher.

## Technical Details

This modlet includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, this modlet must be installed on both servers and clients.

While this modlet does not contain custom C# code, the modlets it depends upon do.
You **must** run 7 Days To Die with **EAC off.**

### Biome Spawning

Both Rogues and Psychos are spawned into biome spawns.

Rogues spawn into:
  * the winter biome, if their clothing is appropriate
  * the desert biome, if their clothing is inappropriate for the winter biome
  * cities, both non-forest and forest, day and night

Psychos spawn into:
  * the wasteland biome, including cities, day and night
  * the desert, at low probabilities

Rogues are also spawned into the NPC Core entity groups used by NPC sleeper volumes in POIs:

* `npcBandits*`
* `npcEnemy*`

Psychos are not spawned into NPC Core sleeper volume groups,
They are only spawned into biome spawns and the new Psycho-specific sleeper volume groups.

### Biome decorations

This modlet also comes with prefabs that are used as biome decorations:

* Rogue camps, which spawn in the pine forest, desert, and (occasionally) snow biomes
* Psycho remnants, which spawn in the wasteland biome and (occasionally) desert biomes

If you do not want them to spawn, remove or comment out the XML in `biomes.xml`.
You can also adjust the spawn probabilities in that file.

These prefabs use the new sleeper volume groups (see below).
If you are a prefab designer, you can look at them to see how the groups are used.

### New Sleeper Volume Groups

This modlet also comes with new Rogues and Psychos sleeper volume groups.
These groups will spawn _only_ Rogues, or _only_ Psychos, and not any other kind of bandit.

To use these groups:

1. Open the Prefab editor in 7D2D.
2. Choose the prefab into which you would like to spawn only Rogues or Psychos.
    (You can copy an existing prefab, or create an entirely new one if you prefer.)
3. For each sleeper volume in the prefab, select the prefab, and hit the `k` key.
4. In the group search bar, enter either "rogue" or "psycho" as appropriate.
    This will result in a few different sleeper volume groups dedicated to either rogues or psychos.
5. Click on the appropriate sleeper volume group.
    The "Group" value in the upper-right hand corner of the screen should change to the group you selected.
6. When done, save the prefab.

These are the available sleeper volume groups.

Rogues:
* Group NPC Rogues All
* Group NPC Rogues Melee
* Group NPC Rogues Ranged
* Group NPC Rogues Boss

Psychos:
* Group NPC Psychos All
* Group NPC Psychos Melee
* Group NPC Psychos Ranged
* Group NPC Psychos Boss

> :warning: If you make POIs with these sleeper volume groups,
> they **will throw exceptions** if this modlet is not installed.
> If you are distributing those POIs to other people,
> you should either distribute this modlet as well,
> or make it _very_ clear that they will need to install this modlet themselves.

### Custom Wandering Horde Groups

This mod also includes custom spawn groups for wandering hordes.
There are separate spawn groups for Rogues and for Psychos.
Each set of spawn groups contains one group per wandering horde game stage.

These wandering horde spawn groups are not enabled by default.
To enable them, change the "WanderingHorde" spawners in `gamestages.xml`,
so they use a Rogue or Psycho spawn group instead of vanilla.

For example, to replace wandering horde for game stage 8 with Rogues:
```xml
<set xpath="//spawner[@name='WanderingHorde']/gamestage[@stage='08']/spawn/@group">wanderingRogueHordeStageGS08</set>
```

There are other examples in this mod's `gamestages.xml` file, but they are commented out.

> :warning: Hordes **cannot** spawn NPC Core characters that use the `EntityAliveSDX` class.
> They must use the `EntityEnemySDX` class, or one of the vanilla enemy classes.
> Don't put the "advanced" versions of Rogues or Psychos into these groups!

### Basic and Advanced Versions

There are two versions of each set of characters:

* Basic characters, which do not have dialog options and can not be hired.
    They can also be spawned into horde groups (wandering or blood moon).
* Advanced characters, which have dialog interactions, and can be hired,
    assuming your standing with the Bandits faction is good enough.
    However, they can **not** be spawned into horde groups (wandering or blood moon) -
    if you try this, the game will throw an `InvalidCastException`.

By default, I am assuming most players will want the basic versions.
Those are the versions spawned into all entity groups, if you don't modify the XML.

The advanced versions are named the same as the basic versions, except with an "npc" prefix.
So, to spawn the advanced versions instead,
use XPath to insert the prefix before the names of all entities in the groups.

There is already XML in the `entitygroups.xml` file which will do all of this.
Simply un-comment the relevant parts of the XML.

### Non-Enemy Advanced Versions

If you want to make the "advanced" characters "friendly" characters,
simply change the faction of the "advanced" versions to "whiteriver".

The XML to do this is in `entityclasses.xml` but is commented out.

For these characters to spawn, you still need to modify `entitygroups.xml`.
To spawn them into biome spawns, see above.
To spawn them into prefab groups, un-comment the XML at the end of `entitygroups.xml`.

If you are using the Whiteriver versions _instead of_ the bandit versions,
you should also comment out or remove the XML that spawns the bandit versions.
This should also be done in `entitygroups.xml`.

If you are using the Whiteriver versions _in addition to_ the bandit versions,
it would probably be a good idea to halve the spawn probabilities of all characters,
when they spawn into _biome_ spawn groups.
Otherwise, the NPCs have a good chance of over-spawning relative to vanilla zombies or animals.

### How the characters were created

The character models were created using Mixamo Fuse - a free program for making human-like characters.
Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
But it can still be used, and is still very good.

Once the models were created, I exported them to `.obj` files, and rigged them in
[Mixamo](https://www.mixamo.com).
From Mixamo, I exported them to Unity `.fbx` files.

I then imported them into Unity to set up ragdolls, rigging, tags, material shaders, etc.

None of this would be possible were it not for the help of Xyth and Darkstardragon - thank you!

If you would like to know how to do all this yourself,
Xyth has an excellent series of tutorials on YouTube:

https://www.youtube.com/c/DavidTaylorCIO/playlists

### Re-use in other mods/modlets

Any rights that I hold in these characters, I hereby place into the public domain (CC0).
Feel free to re-use them in any of your mods or modlets.

However, I don't hold the rights to all the assets in the models.
I have used assets from Mixamo Fuse, and do not hold any rights in those assets.
Those rights are retained by Fuse.

This sounds worse than it is.
Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
though you cannot repackage and sell the raw Fuse assets
(such as the body part models or individual clothing textures).
I do not provide those raw assets (and wouldn't know how), so this should not be a problem.

The characters also include assets provided by either NPC Core or The Fun Pimps.
These include, but are not limited to:

* Sounds and audio dialog
* Weapons and weapon models
* Animations

I can neither grant nor withhold permission to use these assets.

Because of this, use of these characters must abide by the requirements of **both** NPC Core
and The Fun Pimps.

If you are using them in your own mod or modlet for 7 Days To Die,
then this should not be a problem.
