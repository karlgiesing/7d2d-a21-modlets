# Notes
(Don't check this in to the repo)

## Default phrases (existing)

"JoinMe","I would like you to join me." - was: "Hire"
"FollowMe","Follow me."
"TellMe","Tell me about yourself."
"ShowMe","Show Me your inventory."
"StayHere","Stay here."
"GuardHere","Guard here."
"Wander","Wander around here."
"SetPatrol","Follow me for your patrol route."
"SetCode","Here is your Patrol Code."
"Loot","Go find loot for me."
"Dismiss","You are dismissed from my service."
"Patrol","Patrol your route."
"done","Never mind."
"LetsTalk","Let's talk."

### Refactored: start responses

"LetsTalk","Let's talk."
    - statement: conversation
"JoinMe","I would like you to join me."
    - requirement: not hired
    - action: open "hire" dialog
"ShowMe","Show Me your inventory."
    - requirement: hired
    - action: execute "OpenInventory" command
"FollowMe","Follow me."
    - requirement: hired
    - action: execute "FollowMe" command
"StayHere","Stay here." (Change to "Wait here"?)
    - requirement: hired
    - action: execute "StayHere" command
"Dismiss","You are dismissed from my service."
    - requirement: hired
    - statement: dismiss
"done",dialog_trader_response_nevermind

#### Refactored: dismiss statement
"Are you sure?"

"Yes"
    - action: execute "Dismiss" command
"No"
    - statment: start

### Refactored: task responses
"What would you like me to do?" (normal)
"Yes, sir." (soldier)
"Jawoll, mein Führer!" (bandit)

"GuardHere","Guard here."
"Wander","Wander around here."
"SetPatrol","Follow me for your patrol route."
"Patrol","Patrol your route."
"SetCode","Here is your Patrol Code." (Does this work?)
"Loot","Go find some loot." (Does this work?)

### Refactored: conversation responses
"What would you like to know?"

* "TellMe","Tell me about yourself."
    * action: execute "OpenInventory" command
* (before) "What did you do before the apocalypse?"
* (group) "What happened to your group?"
* (family) "Do you have any family?"
    * (parents) "What happened to your parents?"
    * (siblings) "Did you have any siblings?"
    * (spouse) "Did you have a spouse?"
    * (children) "Did you have any children?"
* (survive) "How did you survive on your own?" - maybe, only after all others have been asked?

## Common Phrases

"Shit happens."
"Fuck off."
"Fuck you."
"Well fuck you too."
"None of your business."
"Thank you."
"You're welcome."
"That's terrible!"
"I'm sorry to hear that."
"I'm sorry."
"I meant no offence."
"I agree."
"..."
"They didn't make it."
"He died."
"She died."
"I don't want to talk about it."
"Let's talk about something else."
"Yes."
"No."
"OK."
"I have a task for you."
"What would you like me to do?"
"Why?"
"Why not?"
"What happened?"

## Quotes

### Bible
_(All translations English Standard Version unless otherwise noted)_

"Your dead shall live; their bodies shall rise. You who dwell in the dust, awake and sing for joy! For your dew is a dew of light, and the earth will give birth to the dead." - Isaiah 26:19

"And this shall be the plague with which the LORD will strike all the peoples that wage war against Jerusalem: their flesh will rot while they are still standing on their feet, their eyes will rot in their sockets, and their tongues will rot in their mouths." - Zechariah 14:12

"My bones stick to my skin and to my flesh, and I have escaped by the skin of my teeth." - Job 19:20

"I am the resurrection and the life; he who believes in Me will live even if he dies, and everyone who lives and believes in Me will never die." - John 11:25-26

"For the love of Christ controls us, having concluded this, that one died for all, therefore all died; and He died for all, so that they who live might no longer live for themselves, but for Him who died and rose again on their behalf." - 2 Corinthians 5:14-15

"For since by man came death, by man came also the resurrection of the dead." - 1 Corinthians 15:21 - KJV

"For one who has died has been set free from sin." - Romans 6:7

"Then God blessed Noah and his sons, saying to them, "Be fruitful and increase in number and fill the earth. 2 The fear and dread of you will fall on all the beasts of the earth, and on all the birds in the sky, on every creature that moves along the ground, and on all the fish in the sea; they are given into your hands. 3 Everything that lives and moves about will be food for you. Just as I gave you the green plants, I now give you everything." - Genesis 9:1-3

#### Snipe hunt note?
(also included: bone knife and rotten flesh)

"In all toil there is profit, but mere talk tends only to poverty." - Proverbs 14:23

"And put a knife to your throat, if you are a man of great appetite." - Proverbs 23:2 (New American Standard Bible)

### Johnny Cash
"How well I have learned that there is no fence to sit on between heaven and hell. There is a deep, wide gulf, a chasm, and in that chasm is no place for any man." 

"There was nothing left of me. I had drifted so far away from God and every stabilizing force in my life that I felt there was no hope…My separation from Him, the deepest and most ravaging of the various kinds of loneliness I'd felt over the years, seemed finally complete. It wasn't. I thought I'd left Him, but He hadn't left me. I felt something very powerful start to happen to me, a sensation of utter peace, clarity, and sobriety. Then my mind started focusing on God."

"Trust gets you killed, love gets you hurt and being real gets you hated." - attributed

"I found out that there weren’t too many limitations, if I did it my way."

"God gives us life and takes us away as He sees fit."

### Other Quotes

#### Murder

"I have no interest in understanding sheep, only eating them." - Hannibal Lecter

"Cruelty Is A Gift Humanity Has Given Itself." - Hannibal Lecter

"Killing don’t need no reason. This is ghetto. Reason is for rich people. We have madness." - Marlon James, A Brief History of Seven Killings

"We read of killing one hundred thousand men in a day. We read about it and we rejoiced in it -- if it was the other fellows who were killed. We were fed on flesh and drank blood. Even down to the prattling babe. I need not tell you how many upright, honorable young boys have come into this court charged with murder, some saved and some sent to their death, boys who fought in this war and learned to place a cheap value on human life. You know it and I know it. These boys were brought up in it. The tales of death were in their homes, their playgrounds, their schools; they were in the newspapers that they read; it was a part of the common frenzy -- what was a life? It was nothing. It was the least sacred thing in existence and these boys were trained to this cruelty." - Clarence Darrow

"The joy of killing! the joy of seeing killing done - these are traits of the human race at large." - Mark Twain, Following the Equator: A Journey Around the World

"Evil is a point of view. We are immortal. And what we have before us are the rich feasts that conscience cannot appreciate and mortal men cannot know without regret. God kills, and so shall we; indiscriminately He takes the richest and the poorest, and so shall we; for no creatures under God are as we are, none so like Him as ourselves, dark angels not confined to the stinking limits of hell but wandering His earth and all its kingdoms." - Anne Rice

"Trample the weak. Hurdle the dead." - Attila

"For what is war but your usual custom? Or what is sweeter for a brave man than to seek revenge with his own hand? It is a right of nature to glut the soul with vengeance. Let us then attack the foe eagerly; for they are ever the bolder who make the attack. Despise this union of discordant races! To defend oneself by alliance is proof of cowardice. See, even before our attack they are smitten with terror. They seek the heights, they seize the hills and, repenting too late, clamor for protection against battle in the open fields. (...) Let the wounded exact in return the death of his foe; let the unwounded revel in slaughter of the enemy. No spear shall harm those who are sure to live; and those who are sure to die Fate overtakes even in peace." - Attila

"Show me where the filthy human is so that I may feed him his organs." - Eoin Colfer, The Time Paradox

"It's more useful to have someone fear you than respect you." - J. Cornell Michel, Jordan's Brains: A Zombie Evolution

"In the end, we all die, and nothing really matters." - Richard Ramirez

"We've all got the power in our hands to kill, but most people are afraid to use it. The ones who aren't afraid, control life itself." - Richard Ramirez

"Wow. What a trip! I thought, ‘To taste death, and yet give life.’ Have you ever tasted blood? It’s warm and sticky and nice." - Susan Atkins

"I love looking at dying people. The last smile on a dying face gives me a great thrill." - Marie Emilie Raymond

"If you would gain a throne and hold it, fear not to make of human skulls thy stepping stones." - Taitu Betul, attributed

"Harden thy heart to all pity, all remorse; then shall thy mind and heart be free, without scruple, to gain high aims." - Taitu Betul, attributed

"A heart that is without tenderness of mercy alone can inhabit a body able to endure and to suffer all." - Taitu Betul, attributed

"To me, this world is nothing but evil, and my own evil just happened to come out cause of the circumstances of what I was doing." - Aileen Wuornos

#### Military/Warfare

"As George Orwell pointed out, people sleep peacefully in their beds at night only because rough men stand ready to do violence on their behalf." - Richard Grenier

"God made them as stubble to our swords." - Oliver Cromwell

"Violence, naked force, has settled more issues in history than has any other factor." - Starship Troopers

"The West won the world not by the superiority of its ideas or values or religion (...) but rather by its superiority in applying organized violence." - Samuel P. Huntington, The Clash of Civilizations and the Remaking of World Order

"The more comfort, the less courage there is." - Alexander Suvorov

"The Church will pray to God for the dead. The survivor has honor and glory." - Alexander Suvorov

"Accustom yourself to tireless activity." - Alexander Suvorov

"Brave men rejoice in adversity, just as brave soldiers triumph in war." - Lucius Annaeus Seneca

"Shut the fuck up, Carl." - meme

#### Cooking

"The only real stumbling block is fear of failure. In cooking you’ve got to have a what-the-hell attitude." - Julia Child

"Always start out with a larger pot than what you think you need" - Julia Child

"I approach cooking from a science angle because I need to understand how things work. If I understand the egg, I can scramble it better. It’s a simple as that." - Alton Brown

"No rules. Don't be afraid to do whatever you want. Cooking doesn't have to have rules. I don't like it that way." - Masaharu Morimoto

"Remember, it is never the knife's fault." - Daniel Boulud

"It's okay to play with your food." - Emeril Lagasse

"Working in a restaurant means being part of a family, albeit usually a slightly dysfunctional one. Nothing is accomplished independently." - Joe Bastianich

"I always say that I don't believe I'm a chef. I try to be a storyteller." - Jose Andres

#### Farming

"There are only three things that can kill a farmer: lightning, rolling over in a tractor, and old age." – Bill Bryson

"The ultimate goal of farming is not the growing of crops, but the cultivation and perfection of human beings." – Masanobu Fukuoka

"Farming is a matter of dirt and dung. It is not the kind of thing we look to to find the meaning of human life. It is too ordinary, too inescapably a part of life to be interesting. We know that it has to be done, but see no reason to pay much attention to it. But it is just because farming is inescapably a part of human life that it may provide a clue to what is most basically human, and so a clue to our place within the cosmos." – Stephanie Nelson

"My grandfather used to say that once in your life you need a doctor, a lawyer, a policeman and a preacher, but every day, three times a day, you need a farmer." – Brenda Schoepp

"I enjoyed every bit of it — the responsibility, the successes and dealing with everything that came up. There was a real sense of satisfaction in it." – Bran McCoy, Refuge Dairy

#### Individualism (or not)

"As for me, I am mean: that means that I need the suffering of others to exist. A flame. A flame in their hearts. When I am all alone, I am extinguished." - Jean-Paul Sartre, No Exit

"It's the reductionist approach to life: if you keep it small, you'll keep it under control. If you don't make any noise, the bogeyman won't find you. But it's all an illusion, because they die too, those people who roll up their spirits into tiny little balls so as to be safe. Safe?! From what? Life is always on the edge of death; narrow streets lead to the same place as wide avenues, and a little candle burns itself out just like a flaming torch does. I choose my own way to burn." - Sophie Scholl

#### "Marriage" by Gregory Corso
Should I get married? Should I be good?
Astound the girl next door with my velvet suit and faustus hood?
**Don't take her to movies but to cemeteries**
**tell all about werewolf bathtubs and forked clarinets**
then desire her and kiss her and all the preliminaries
and she going just so far and I understanding why
not getting angry saying You must feel! It's beautiful to feel!
Instead take her in my arms lean against an old crooked tombstone
and woo her the entire night the constellations in the sky-

When she introduces me to her parents
back straightened, hair finally combed, strangled by a tie,
should I sit with my knees together on their 3rd degree sofa
and not ask Where's the bathroom?
How else to feel other than I am,
often thinking Flash Gordon soap-
O how terrible it must be for a young man
seated before a family and the family thinking
We never saw him before! He wants our Mary Lou!
After tea and homemade cookies they ask What do you do for a living?

Should I tell them? Would they like me then?
Say All right get married, we're losing a daughter
but we're gaining a son-
And should I then ask Where's the bathroom?

O God, and the wedding! All her family and her friends
and only a handful of mine all scroungy and bearded
just wait to get at the drinks and food-
And the priest! he looking at me as if I masturbated
asking me Do you take this woman for your lawful wedded wife?
**And I trembling what to say say Pie Glue!**
I kiss the bride all those corny men slapping me on the back
She's all yours, boy! Ha-ha-ha!
And in their eyes you could see some obscene honeymoon going on-
Then all that absurd rice and clanky cans and shoes
Niagara Falls! Hordes of us! Husbands! Wives! Flowers! Chocolates!
All streaming into cozy hotels
All going to do the same thing tonight
The indifferent clerk he knowing what was going to happen
**The lobby zombies they knowing what**
**The whistling elevator man he knowing**
**Everybody knowing! I'd almost be inclined not to do anything!**
Stay up all night! Stare that hotel clerk in the eye!
Screaming: **I deny honeymoon! I deny honeymoon!**
running rampant into those almost climactic suites
yelling **Radio belly! Cat shovel!**
O I'd live in Niagara forever! in a dark cave beneath the Falls
I'd sit there the Mad Honeymooner
devising ways to break marriages, a scourge of bigamy
a saint of divorce-

But I should get married I should be good
How nice it'd be to come home to her
and sit by the fireplace and she in the kitchen
aproned young and lovely wanting my baby
and so happy about me she burns the roast beef
and comes crying to me and I get up from my big papa chair
saying **Christmas teeth! Radiant brains! Apple deaf!**
God what a husband I'd make! Yes, I should get married!
So much to do! Like sneaking into Mr Jones' house late at night
and cover his golf clubs with 1920 Norwegian books
Like hanging a picture of Rimbaud on the lawnmower
like pasting Tannu Tuva postage stamps all over the picket fence
like when Mrs Kindhead comes to collect for the Community Chest
grab her and tell her There are unfavorable omens in the sky!
And when the mayor comes to get my vote tell him
When are you going to stop people killing whales!
And when the milkman comes leave him a note in the bottle
**Penguin dust, bring me penguin dust, I want penguin dust-**

Yes if I should get married and it's Connecticut and snow
and she gives birth to a child and I am sleepless, worn,
up for nights, head bowed against a quiet window, the past behind me,
finding myself in the most common of situations a trembling man
knowledged with responsibility not twig-smear nor Roman coin soup-
O what would that be like!
Surely I'd give it for a nipple a rubber Tacitus
For a rattle a bag of broken Bach records
Tack Della Francesca all over its crib
Sew the Greek alphabet on its bib
And build for its playpen a roofless Parthenon

No, I doubt I'd be that kind of father
Not rural not snow no quiet window
but hot smelly tight New York City
seven flights up, roaches and rats in the walls
a fat Reichian wife screeching over potatoes Get a job!
And five nose running brats in love with Batman
And the neighbors all toothless and dry haired
like those hag masses of the 18th century
all wanting to come in and watch TV
The landlord wants his rent
Grocery store Blue Cross Gas & Electric Knights of Columbus
**impossible to lie back and dream Telephone snow, ghost parking-**
No! I should not get married! I should never get married!
But-imagine if I were married to a beautiful sophisticated woman
tall and pale wearing an elegant black dress and long black gloves
holding a cigarette holder in one hand and a highball in the other
and we lived high up in a penthouse with a huge window
from which we could see all of New York and even farther on clearer days
No, can't imagine myself married to that pleasant prison dream-

O but what about love? I forget love
not that I am incapable of love
It's just that **I see love as odd as wearing shoes-**
I never wanted to marry a girl who was like my mother
And Ingrid Bergman was always impossible
And there's maybe a girl now but she's already married
And I don't like men and-
But there's got to be somebody!
Because what if I'm 60 years old and not married,
all alone in a furnished room with pee stains on my underwear
and everybody else is married! All the universe married but me!

Ah, yet well I know that were a woman possible as I am possible
then marriage would be possible-
Like SHE in her lonely alien gaud waiting her Egyptian lover
so i wait-bereft of 2,000 years and the bath of life.

#### Mad Max

"I am the Nightrider. I’m a fuel-injected suicide machine. I am a rocker, I am a roller, I am a out-of-controller! I’m the Nightrider, baby!" — Nightrider (Mad Max)

"I am the chosen one. The mighty hand of vengeance, sent down to strike the unroadworthy! I’m hotter than a rollin’ dice. Step right up, chum, and watch the kid lay down a rubber road, right to FREEDOM!" — Nightrider (Mad Max)

"Joviality is a game of children." - Bubba Zanetti (Mad Max)

"Remember him when you look at the night sky." - Toecutter, on the death of Nightrider (Mad Max)

"My life fades. The vision dims. All that remains are memories.

I remember a time of chaos, ruined dreams; this wasted land...

(T)he world was once powered by the black fuel, and the desert sprouted great cities of pipe and steel. Gone now, swept away.

For reasons long forgotten, two mighty warrior tribes went to war and touched off a blaze which engulfed them all. Without fuel they were nothing. They had built a house of straw. The thundering machines sputtered and stopped. Their leaders talked, and talked, and talked. But nothing could stem the avalanche. Their world crumbled. Cities exploded; a whirlwind of looting, a firestorm of fear. Men began to feed on men.

On the roads it was a white-line nightmare. Only those mobile enough to scavenge, brutal enough to pillage would survive. The gangs took over the highways, ready to wage war for a tank of juice. And in this maelstrom of decay, ordinary people were battered and smashed." - narrator intro (Mad Max 2: The Road Warrior)

## Personalities

### Misc. Stories

"We were looting a house for food, and were surrounded by a horde from outside. I made it out. They're probably dead."

"My father died before the dead rose. My mother was bitten last year. She got infected, and was about to turn. I had to put her down."

### Chef

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) 

### Cowboy

based on Johnny Cash?

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) 

### Farmer

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) "There are only three things that can kill a farmer: lightning, rolling over in a tractor, and old age."

### Nurse

* (before) "This is not a real nurse's uniform, let's leave it at that."
* (group) Became a nurse for real after apocalypse, worked in refugee camp
    * ("What happened?") Everyone she saved was wiped out during first blood moon
* (family) "I don't want to talk about it."
* (survive) "Most people will make deals with you if you can help them."
    * ("And if not?") "You have to find other ways to deal with them."

### Medic

* (before) Joined military during last days of the war, right before bombs dropped/dead rose
* (group) Served in her unit as a paramedic
    * (what then?) Everyone she saved was wiped out during first blood moon
* (family) "Do you have any family"
    * (parents) "They died before the war."
        * ("I'm sorry to hear that.") "I think they were the lucky ones."
    * (siblings) "I had two brothers in the military, but they were deployed to other units."
        * ("What happened?") "They're probably dead now."
            * ("I'm sorry to hear that.") "Shit happens."
    * (spouse) "Nobody could deal with the fact that I was stronger than them."
    * (children) "This is not a world for children."
* (survive) "Most people will make deals with you if you can help them."
    * ("And if not?") "You have to find other ways to deal with them."

### Bandit/Raider

* (before):
    * Not hired: "There was nothing before the apocalypse."
    * Hired: "I was a high school teacher."
        * ("What happened?") "When the world is ruled by death, only a fool would not become a bringer of death."
* (group) "To defend oneself by alliance is proof of cowardice."
* (family) "I was fed on flesh and drank blood. Even as a prattling babe. What is a life? It is nothing. It is the least sacred thing in existence. You know it and I know it."
* (survive) "We've all got the power in our hands to kill. The ones who aren't afraid to use it, control life itself."

### Female Bandit/Raider
_(currently only Eve)_

based on Inez from No Exit?

* (before) ???
* (group) ???
* (family) ???
* (survive) "A heart that is without tenderness of mercy alone can inhabit a body able to endure and to suffer all."

### Female Rogue Names

#### Old West
Pearl Hart
Laura Bullion, a.k.a. the Rose of the Wild Bunch
Belle Siddons, a.k.a. Madam Vestal
Rose Dunn, a.k.a. Rose of the Cimarron
Sarah Jane Newman, a.k.a. Sally Scull
Mary Katherine Haroney, a.k.a. Big Nose Kate
Belle Starr
Etta Place
Eleanor Dumont, a.k.a. Madame Mustache
Bonnie Parker

Belle,Bonnie,Eleanor,Etta,Jane,Kate,Katherine,Laura,Mary,Pearl,Rose,Sarah

#### TWD
Mary (Terminus)
Jadis (Scavengers - name too recognizable)
Martha (Fear TWD - she's black, but never mind)
Celia (Fear TWD)
Haley (Governor)
Karen (Governor)
Lizzie ("Look at the flowers")
Theresa (Terminus/Hunters)
Paula (Saviors - young)
Molly (Saviors - older)
Michelle (Saviors - young)

Celia,Haley,Karen,Lizzie,Martha,Mary,Michelle,Molly,Paula,Theresa

### Male Rogue Names

#### Claimers
Joe
Tony
Dan
Billy
Harley
Len
Lou

Billy,Dan,Harley,Joe,Len,Lou,Tony

#### Terminus/Hunters

Chuck
Martin
Greg
Gareth
Albert
Mike
Alex

Albert,Alex,Chuck,Gareth,Greg,Martin,Mike

#### Blonde dude

**Dolph Lundgren:**
Ivan (Drago - Rocky)
Gunner Jensen (Expendables)
Frank (Castle aka Punisher)
Andrew Scott (Universal Soldier)
Konstantin (Kovar - Arrow [yes that one])
Gil (mature version of character from Sharknado 5)
John (Thundergun - It's Always Sunny)
Adam (He-Man)

**Other**
Nada ("They Live")
Sam (Hell, "Hell Comes To Frogtown")
Jon (St. John, voice of Duke Nukem)
John (Cena, is supposed to be playing Duke Nukem in an upcoming film)
Nathaniel (Esquire, joke real name of Duke Nukem)

Adam,Andrew,Frank,Gil,Gunner,Ivan,Jensen,Jon,John,Konstantin,Nathaniel,Sam,Scott

#### Cowboys

Jesse James
Bill(y the Kid)
Butch Cassidy
Harry Alonzo (Longabaugh, the Sundance Kid)
John Wesley (Hardin, racist and generally an asshole)
Sam (Bass)
James/Jim (Brown Miller, Texas Ranger turned outlaw)

Alonzo,Bill,Butch,Cassidy,Harry,James,Jesse,Jim,John,Sam,Wesley

### Psychos

* Bruiser - brute, large female ~~(melee only)~~
* Goliath - brute
* Lunatic - skinny female, skinny male
* Marauder - large female
* Nomad - large female
* Outlaw - skinny female, skinny male
* Raider - skinny female, skinny male
* Rat - skinny male
* Scavenger - skinny female, skinny male
* Vic - skinny female (BL3 head in a jar)

#### Psycho Sound Notes

* Non-brutes: use "death" sounds for random
* Non-brutes: use "painlg" sounds for alert
* Don't use "malehate" for psychos - only for (male) rogues
* Brutes: use custom "psychobrute" sounds, which are "malehate" pitched lower


### Whisperer

* ("Why do you walk with the dead?")
    * "God has blessed Them, saying to Them: Be fruitful and increase in number and fill the earth. The fear and dread of You will fall on all the beasts of the earth, and on every creature that moves along the ground; they are given into Your hands. Everything that lives and moves about will be food for You."
    * ~"One who has died has been set free from sin."~ - used in "murder" quest

* (before) "This was the judgment: the light did come into the world, and people loved the darkness rather than the light because their works were evil. But whoever does what is true comes to the light."
* (group) "The wind blows where it wishes, and you hear its sound, but you do not know where it comes from or where it goes. So it is with everyone who is reborn of the dead souls."
* (family) "Their love controls us, having concluded this, that They died for all, therefore all died; and They died for all, so that those who live might no longer live for themselves, but for Them who died and rose again on their behalf."
* (survive) "My bones stick to my skin and to my flesh, and I have escaped by the skin of my teeth."

### Whisperer Beta

* (before) "I was a famous country music singer."
    * ("Really?") "Just kidding, that would be ridiculous."

### Whisperer Audio
Audio cues for whisperers.

#### SoundRandom
(in addition to zombie sounds)

* Quiet
* Shhhh
* *sigh*
* (indistinguishable)
* "zombie moans"

#### SoundAlert

* I see them
* Over there
* Hey

#### SoundSense

* Wait
* Where
* What WAS that
* I heard something
* (whistling signal)

<!-- ### Downloaded

Female:
* 74417__tim-kahn__eyes-wisp
* 89335__tim-kahn__i-miss-you (edit out "I")
* 131267__bettsashl__didyou (but mix to mono)
* 163289__angeljs__i-will-kill-you-scary-whisper (needs editing) -->

### Whisperer Actor Names

#### Male
* 1: Bret
* 2: Evan
* 4: Seth
* 5: Ben
* 7: Victor
* 10: Ross
* 11: James
* 12: Patrick
* 13: William Gregory Lee
* 14: Chris
* 15: Alex
* 16: Stefan
* 18: Philip
* 20: Nate
* Rufus: Mark
* Adam (child): Noah, Preston
* Keith (post-Alpha): Brad

teen:
Adam,Noah,Preston

adult:
Alex,Ben,Brad,Bret,Chris,Evean,Gregory,James,Keith,Lee,Mark,Nate,Patrick,Philip,Ross,Rufus,Seth,Stefan,Victor,William

#### Female
* 3: Kristin
* 6: Terri
* 8: Emma
* 9: Melanie
* 17: Jessica
* 19: Grace
* Lydia: Cassady, Scarlett (child), Havana (pre-teen)
* Gamma: Thora / Mary (TV)

Cassady,Emma,Grace,Havana,Jessica,Kristin,Mary,Melanie,Scarlett,Terri,Thora

#### Alpha
* Samantha (actress)
* Alfie (TV - I guess? Can't find where I found that)
* Adelpha (similar)
* Aleksandra (similar)
* Nyx (similar - primordial Greek goddess of night, mother of the Fates)
* Aisa (aka Atropos - eldest of the Fates, cuts the strings)
* Eris (Greek goddess of strife)

#### Beta
* Ryan (Hurst, who also plays...)
* Opie (...on Sons of Anarchy, whose "real" first name is...)
* Harry
* "Half Moon" (TV, was inspired by...)
* Garth (Brooks, who sometimes performed as...)
* Chris (Gaines)

Ryan,Harry,Garth,Chris

### Vault dwellers
~~(N/A - can't converse)~~ _Can in A20_


## AI Generated Text
_Note:_ Many of the phrases I am feeding the generators are quotes from Psychos in Borderlands.
I was mainly trying to generate word salad for Psychos, but some could be useful elsewhere.

### DeepAI
https://deepai.org/machine-learning-model/text-generator

* _Every morning I cry, because the moon IS DEAD!_
    > I get up every morning and cry. **I have no memory for when it felt like my body was dead.** How often did I remember that? Every day they're talking about this, how much worse it could've looked.
    > I go to sleep and cry. I need your help.
    > **The morning of the accident, I was watching the sky through my phone as I was driving across town. I tried and failed to stay awake, but I couldn't stop crying.**
    > I was going to be there and see if I could see people. **People were dying, so I kept asking myself: How did I feel and who was trying to help? How could I do something to help?**
    > On my way home from work I stopped to ask, "My dad says you're going to have to have my baby right away."
    > He said. "I hate talking about it!" He was right.
    > And when we started to make our way around town around six in the morning, I was thinking, "This is how you know a baby is dead?"
    > In all of those years, I'm never in a situation where you could have an unspoken, simple answer to that question.
    > I'm sure there are some who call this "being too emotional" which seems the opposite. But the truth is that I get emotional when someone feels this way, whether it's me

* _I'm not a cannibal, you're an animal!_
    > Karen: What're you going to do here? I need to find some meat. I need to get some meat. So I can be a good man for the people of our ranch! [kissing sounds] So… you want to go back, then? Tell me what your wife has to think.
    > Evan: You know, I'm the best girl in the family. She said she's not going back to that life.
    > Donna: **Just to show ya, honey, I'm not gonna hurt you, you know? I'm not a vegetarian, I'm not a cow, I'm not a mare.** (laughs) Oh you know I don't know shit about being a faggot, I just love the animal side….
    > Donna: You're my friend! How will you behave if you don't?
    > Evan: [sighs] I'm a bit of a tease. She said that you guys were the best team up they'd ever known and were all over my family, all over each other. How about you, have a seat at the table and let me get this fucking shit off my chest? Just leave me here or if that doesn't work out then I'll come in.
    > [laughter]
    > Evan: You know what it's gonna get you when I get you back in here!

* _Don't stare at the entrance to my organ jail!_
    > What a nice place to spend a day!
    > Why did I leave you here?
    > Don't try to leave me here, I'm waiting for you
    > What if I tell you that they are not here
    > "Why?"
    > "They shouldn't know. And they weren't with me when they tried."
    > "Why? Why did you just leave me out like that?"
    > I see he had to see this. He'd seen every one of us, from him. He just wanted to say, "Oh my God! I can't believe you couldn't leave me out. Why is it like that?"
    > What a weird feeling I'm having from the thought of him leaving me out. Don't worry, I'll help you out soon.
    > **You'll have a good time in hell, I promise.**
    > Are you alright, my angel?

* _I plucked a hair from the head of a dying baby! Let me give it to you!_
    > (To the crying baby, who, after all this time, will not give him food or water)
    > **"How many years have you been there?** I saw the children in the courtyard! They had come without any food or water.
    > **My mother, I saw them naked, with their heads covered with dead clothes.**
    > There was no way for me to tell them to put some food on the table.
    > This was a very sad, horrible thing. I couldn't see them much. They could only live where I looked.
    > I think I might have been able to help them so much. Can you forgive me if I forget any of this?"
    > The crying baby then took out a piece of paper, and a piece of paper which his mother had brought with him from his home.
    > He began at all the necessary steps on the list.
    > The crying baby then sat down, the crying baby holding his paper and the crying baby sitting on the table.
    > By itself, this was as convincing as it was convincing.
    > The crying baby put his face against the table and the crying baby, as well as several of his clothes on the table, put his head on the table, looking down at the crying baby's face at the end of the list.
    > The crying baby then began to sit on the table, in the chair.
    > The crying baby told him how many times he had cried from this place, and it was

* _Lick and scream, and pee! I'm COMING to make you teeth!!!_
    > How is that not fun!? How's that not fun?!
    (...the rest was lame)

* _Lick and scream, and pee! I'm COMING to make you teeth! Murder!_
    > I'm doing it I want you to get real! What are you doing?! KILLER!
    > **Why don't I pull my knife on you!? Is that okay?!**

### InferKit
https://app.inferkit.com/demo

* _Every morning I cry, because the moon IS DEAD!_
    > You want to know what the worst thing about that is?
    > **The moon IS dead, but you CANNOT let that show!**
    > No.
    > You cannot let that show!
    > **It's MY moon!**
    > I've watched the moon my whole life.
    > No one has helped me, in the same way as I have helped the moon.
    > But the moon is dead, so I can't do anything for it!
    > I suppose I could have started another lantern, for the sun.
    > But the moon is dead, and so the sun is waiting, quietly, behind me, to take over.
    > **And when the new moon comes, I will be there, like a dark, dead shadow to hide the sun.**
    > It is my turn, now, to protect the bright side.
    > My turn, to protect the days of summer.
    > My turn, to cheer the moon, so that it can rise again.

* _I'm not a cannibal, you're an animal!_
    > "We don't eat each other for food!"
    > Sarah recoiled in shock.
    > "What are you talking about? You sound like a fucking psycho!"
    > Anaph calmed down and wiped his face.
    > "Hey, I'm sorry if I offended you. And no, I don't eat cannibalism."
    > Sarah's look of anger faded to fear.
    > "Yeah, thanks, doc. I feel a lot better now."
    > "Yeah, well, **try and keep this up, and I'll have to stick a fucking knife in you myself."**
    > He smiled evilly.
    > He felt an arm wrap around his neck, and he was yanked up off the ground.
    > He let out a yelp as his arm was twisted up and a cord was wrapped around his neck from behind and his head jerked back.
    > As he was being dragged the pain in his arm doubled him over and the

* _I plucked a hair from the head of a dying baby! Let me give it to you!_
    > It is the blood of a healer!" Lusamine nodded, smiling wickedly, "Indeed it is." Then, in a sickeningly human voice she cried out, "Get it, Seolleon!"
    > The blood was putrid, a brown, sour, dark liquid. **What a human would take for comfort, Lusamine found to be terrifying, and disgusting.** A human who did this, a Seolleon who should have been born of the sky, the light, **Lusamine found a predator in this girl.**
    > The Seolleon had collected many hairs from infants as the long-dead child slowly bled away. It was still connected to her with its umbilical cord. Lusamine pulled a few strands of hair from it, and stared at them in the firelight.
    > "My, my." Lusamine spoke in a soft, but dry voice. The head shuddered.
    > "There is something strange about this one. What do you make of it?" He asked.

* _Lick and scream, and pee! I'm COMING to make you teeth! [Note: tried to include "murder"]_
    > **It will end like a cold dog.**
    > HARMONIX IS THE MURDERER!!!
    > She can't get the bloody stone away! The stone of Höllen!!!
    > We just opened the doors to Höllen!
    > HÖLLEN! It can't be real! It's so fucking awful!
    > **GET OUT OF HERE! KILL THEM ALL! KILL THEM ALL!!!**
    > THERE'S NOTHING LIKE TEETH!
    > HERE'S ONE! KICK HIS ASS!
    > **They were so hungry they stole each others lunch!**
    > **A lot of jaws went down in that pig pen!**
    > MURDERER!!!
    > Why did you kill the stupid boy?
    > MURDERERS!
    > Burnt the cows, father! Murderers

### Sassbook AI Writer
https://sassbook.com/ai-writer

* _Every morning I cry, because the moon IS DEAD!_
    > I'd just been told I will be executed. Not executed in the way that humans are executed, I mean, a short sharp jab with a sharp object. No, no, not a jab. A sharp metal object, like a baseball bat. I thought of my brother, who has been dying on and off since he was in first grade. My mom crying all night, my dad fighting with my brothers and me as I yelled "You're going down!"
    > He had a point, though. **I'll be a vegetable like my mother. One of those weird vegetables with extra arms. Probably some kind of fungus. Like the green fungus my grandmother made for Christmas.** Did you get that joke yet?
