# Entity Sounds

What entity sounds are available and what triggers them.

These are defined in `entity_class` properties in `entityclasses.xml`.

These notes are being used to update UAI tasks so they trigger the sounds,
so that is why the SCore utility AI classes are discussed.

## "SoundAlert"

Triggered by:
* `EntityAlive.OnUpdateEntity`

Conditions:
* `EntityAlive.attackingTime ` < 0
  * Set when the entity is attacking
  * No code changes needed
* `EntityAlive.ApproachingEntity` is true
  * Set by `EntityAlive.SetAttackTarget`
    * ~~_which we override_ because we want to avoid using attack targets in NPCs~~
    * ~~**Code changes may be needed** - if we aren't calling the base class then set it to true~~
    * ...turns out we _are_ calling the base class appropriately
  * Set to false in `EntityAlive.OnUpdateEntity` if `EntityAlive.attackingTime` > 0
    * If it's attacking it's no longer "approaching"
    * No code changes needed
* `EntityAlive.IsScoutZombie` is false
  * Set by `AIScoutHordeSpawner.SpawnUpdate` - always true for these entities
  * Unless we want scout hordes to spawn NPCs that will make "alert" sounds, no code changes needed
* `EntityAlive.livingSoundTicks` < 0
  * Set in `EntityAlive` when any sound is played - seems to ensure sounds aren't played together.
  * Uses "SoundAlertTime" property value to add ticks (plus a random amount)
  * No code changes needed
* Entity is not stunned
  * ...self-explanatory I hope
  * No code changes needed
* `EntityAlive.SleeperSupressLivingSounds` is false
  * Set to true in `EntityAlive.TriggerSleeperPose`
  * Set to false in `EntityAlive.ProcessDamageResponseLocal`
  * Set to false in `EAIApproachAndAttackTarget.Update`
    * **Code changes needed** - set to false in UAI "attack" actions

### UAI

* `UAITaskAttackTargetEntitySDX`
* `UAITaskMoveToAttackTargetSDX`
* `UAITaskMoveToTargetSDX`
  * base class of `UAITaskMoveToAttackTargetSDX`
  * has `Start` method

## "SoundSense"

Triggered by:
* `EAISetNearestEntityAsTarget.SeekNoise`
  * **Code changes needed** - figure out the equivalent in UAI (tasks? considerations?)
  * Called by `EAISetNearestEntityAsTarget.FindTarget`
    * Called by `EAISetNearestEntityAsTarget.CanExecute`
    * Called by `EAISetNearestEntityAsTarget.Continue`
  
Conditions:
* `EAISetNearestEntityAsTarget.senseSoundTime - Time.time` < 0
  * `senseSoundTime` is set to random time (10 - 20 seconds?) in this case
* Entity can sense target
  * Distance to target <= (max hear distance of target class * sense scale of entity * `player.DetectUsScale`)

### UAI
Possible candidates:

* `UAITaskMoveToInvestigate`
  * used in "NPCModHostileMeleeBasic", "NPCModHostileRangedBasic" and "NPCModMechRangedBasic"
* `UAITaskMoveToAttackTargetSDX`
  * used in most packages
* `UAITaskMoveToTargetSDX`
  * base class of `UAITaskMoveToAttackTargetSDX`
  * has `Start` method
  
## "SoundGiveUp"
Note: In vanilla, used only with animals.

Triggered by:
