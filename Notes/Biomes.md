# Biomes

Difficulty:

1. pine_forest
    * Rural, day: ZombiesAll
    * Rural, night: ZombiesNight
    * Downtown, day: **ZombiesForestDowntown**
    * Downtown, night: **ZombiesForestDowntownNight**
    * Animals, day: WildGameForest
    * Animals, night: **WildGameForestNight,** **EnemyAnimalsForest**
2. burnt_forest
    * Rural, day: ZombiesAll, **ZombiesBurntForest**
    * Rural, night: ZombiesNight
    * Downtown, day: ZombiesDowntown
    * Downtown, night: ZombiesDowntown
    * Animals, day: WildGameForest
    * Animals, night: WildGameForest, **EnemyAnimalsBurntForest**
3. desert
    * Rural, day: ZombiesAll
    * Rural, night: ZombiesNight
    * Downtown, day: ZombiesDowntown
    * Downtown, night: ZombiesDowntown
    * Animals, day: WildGameForest, **EnemyAnimalsDesert**
    * Animals, night: WildGameForest, **EnemyAnimalsDesert**
4. snow
    * Rural, day: **SnowZombies**
    * Rural, night: **SnowZombies,** ZombiesNight
    * Downtown, day: ZombiesDowntown
    * Downtown, night: ZombiesDowntown
    * Animals, day: WildGameForest, **EnemyAnimalsSnow**
    * Animals, night: WildGameForest, **EnemyAnimalsSnow**
5. wasteland
    * Rural, day: **ZombiesWasteland**
    * Rural, night: **ZombiesWastelandNight**
    * Downtown, day: **ZombiesWastelandDowntown**
    * Downtown, night: **ZombiesWastelandDowntown**
    * Animals, day: **EnemyAnimalsWasteland**
    * Animals, night: **EnemyAnimalsWasteland**