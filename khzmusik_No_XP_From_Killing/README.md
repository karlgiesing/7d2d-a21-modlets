# No XP From Killing

Players do not get any experience points from killing zombies or any other entity.

As a result, the XP notification will not display when a zombie is killed.
You can no longer use that notification to determine if a zombie is really dead.

Experience points are otherwise unchanged;
you still get the same XP from crafting, looting, mining, or doing quests.
You may want to increase the XP multiplier to make up for the lost XP from killing.

I created this mod to lessen the incentive for killing dangerous enemies,
so players will have as munc incentive to avoid them instead.
(It does not _entirely_ remove the incentive for killing enemies;
zombies still drop loot bags, and animals can be harvested.)

This steers the game away from being a first-person shooter,
and closer to being a survival game.

## Features

* No XP from killing.
* No XP notification when entities die.
* New loading screen tip.
* New journal entry, unlocked on killing an entity for the first time.

## Technical Details

This modlet uses XPath to modify XML files.
It does not have any custom C# code, so it should be compatible with EAC.
It does _not_ contain any new assets (such as images or models).
Servers should automatically push the XML modifications to their clients, so separate client
installation should _not_ be necessary.

Starting a new game should not be necessary, but is always recommended just in case.
