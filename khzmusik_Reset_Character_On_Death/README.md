# Reset Character

Resets the player character when they die.

Unlike the Mostly Dead and Permadeath modlets, this is all done using XML edits.

## Features

When the player character dies:

* Skill points are reset and removed.
* Books that were read are forgotten.
* Crafting skill levels are reset to 1.

With XML edits, it can also support these features:

* Bedrolls and beds are no longer spawn points, so players do not spawn on them.
* There is no experience loss upon respawning.
* Resetting can be disabled for level 1 players, or for players that still have the "newbie coat" buff.
* Remembering skill points, books read, or crafting skills.

## Technical Details

This modlet uses XPath to modify XML files, does not contain new assets, and does not use C# code.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients,
so separate client installation should not be necessary.

### Customizing

Most of these features are implemented using XML, but are commented out.
To enable them, remove the XML comment tags (`<!--` and `-->`).

#### Decorative beds and bedrolls

To make bedrolls decorative, you will need to do a few different edits.

First, remove the "SleepingBag" class from all blocks.
This prevents them from being spawn points, and from creating a protected area.
This is done by un-commenting this line in `blocks.xml`:
```xml
<remove xpath="//block/property[@value='SleepingBag']" />
```

To avoid confusing players, you should also replace the "bed group" description.
When put into `blocks.xml`, this code will make beds and bedrolls use the description
"This block can be placed but is of mostly decorative value."
```xml
<set xpath="//block/property[@value='bedGroupDesc']/@value">creativeMenuBlockGroupDesc</set>
```

Next, you should remove the first "Basic Survival" quest (to make a bedroll),
rename the second quest so it gets launched instead,
and reward the player with 100 XP for the second quest to make up for the XP from the first.
This is done by un-commenting these lines in `quests.xml`:
```xml
<remove xpath="/quests/quest[@id='quest_BasicSurvival1']" />
<set xpath="/quests/quest[@id='quest_BasicSurvival2']/reward[@type='Exp']/@value">100</set>
<set xpath="/quests/quest[@id='quest_BasicSurvival2']/@id">quest_BasicSurvival1</set>
```

If you want to include a loading screen tip,
which tells players that beds and bedrolls are purely decorative,
un-comment this line in `loadingscreen.xml`:
```xml
<tip key="loadingTipNoBedrolls" />
```

Finally, you should delete the `Localization.txt` file,
and rename `Localization-bedrolls.txt` to `Localization.txt`.
(I had to make a different file because the CSV format does not support comments.)

#### Removing the XP death penalty

If you want to remove the XP loss from dying, un-comment these lines in the `entityclasses.xml` file:
```xml
<set xpath="//entity_class[@name='playerMale']/effect_group/passive_effect[@name='ExpDeficitPerDeathPercentage']/@value">0</set>
<set xpath="//entity_class[@name='playerMale']/effect_group/passive_effect[@name='ExpDeficitMaxPercentage']/@value">0</set>
```

#### Limiting restarts to players level 2 and above

If you want this behavior, un-comment this line in the "buffResetProgressionOnDeath" buff in `buffs.xml`:
```xml
<requirement name="PlayerLevel" operation="GT" value="1" />
```

You can also choose a different level to prevent resetting, if desired.

#### Limiting restarts to players without the "newbie coat" buff

If you want this behavior, un-comment this line in the "buffResetProgressionOnDeath" buff in `buffs.xml`:
```xml
<requirement name="NotHasBuff" target="self" buff="buffNewbieCoat" />
```

#### Remembering books, skills, or crafting levels

The "ResetProgression" action has a number of XML attributes that determine what is reset and what isn't.

For example:
If you want players, after respawning, to remember the books they read,
set the `reset_books` attribute to "false" on the "ResetProgression" action in `buffs.xml`:
```xml
<triggered_effect trigger="onSelfDied" action="ResetProgression" reset_books="false" reset_crafting="true" reset_levels="true" reset_skills="true" />
```

If you find it easier, you can also remove the attributes instead of setting their values to false.

You can do the same for skill points, skill levels, or crafting levels.
If you use `reset_skills` but not `reset_levels`,
it will behave like the Forgetting Elixir,
and the player will be able to re-spec their character with the skill points they had when they died.
