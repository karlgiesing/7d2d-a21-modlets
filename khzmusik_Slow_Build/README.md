# Slow Build

Slows down building, and to a lesser extent crafting.

This encourages renovating prefabs rather than building structures from scratch,
for both horde bases and personal dwellings.
It also makes inventory management more challenging.
The fact that wood items take nails to craft will also discourage nerd poling.

## Features

* Wood frames, shapes, and ladders now require nails to craft.
  To adjust for this, players are now given a small stack of nails when starting.
  Also, full boxes of nails (100 count) can sometimes be found in loot.
* Crafting cobblestone now requires a cement mixer.
* The stack sizes for all shapes have been reduced to 50 (from 500).
* The stack sizes for natural resources (such as iron or clay) have been reduced from 6000 to 500.
  Stack sizes for cobblestone, cement, and concrete have been reduced from 1000 to 100.
  Bundles of resources have been updated to reflect the new stack sizes.
* Trees now take twice as long to grow, and yield at most one seed.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.
