# Mostly Dead: Permadeath

This modlet turns the "Mostly Dead" modlet into a "Permadeath" modlet.

## Features

This modlet adds new "Permadeath" features:

* Skill points are reset **and removed.**
* Crafting skills are removed _(new for A21)._
* The player level is reset to level 1.
* The player spawns with the starting items again,
  _except_ the note from the Duke.
* The player is given the "Basic Survival" quests again.
* The player is given the "Newbie Coat" buff again.

These "Mostly Dead" features are unchanged:

* The player's inventory is deleted (the "DropOnDeath" setting is forced to "Delete All").
* Books that were read are forgotten.
* Bedrolls and beds are no longer spawn points, so players do not spawn on them.
* The player map is reset, including any saved waypoints.
* Player quests are removed.
* Player vehicles are destroyed.
* The player's active land claim block is rendered inoperable.
* The player's ACL (Access Control List - i.e. friends list) is cleared.
* If the player hired any NPCs, those NPCs are dismissed. (Assumes SCore NPCs.)

## Dependent and Compatible Modlets

This modlet is dependent upon the `khzmusik_Mostly_Dead` modlet.
It was designed and tested to be used with version `21.0.1.0` of that modlet.

> :warning: You must install the `khzmusik_Mostly_Dead` modlet separately.

## Technical Details

This modlet uses XPath to modify XML files, and does not use custom assets or C# code.

However, the `khzmusik_Mostly_Dead` modlet **does** have custom assets and C# code.
It is *not* compatible with EAC,
and both clients and servers must install it.

After installation, it is **highly** recommended to start a new game.

### Installation

You can just drop the `khzmusik_Mostly_Dead_Permadeath` folder into the `Mods` folder,
at the same "level" as the `khzmusik_Mostly_Dead` folder.
