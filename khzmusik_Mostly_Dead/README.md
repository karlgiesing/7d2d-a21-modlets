# Mostly Dead

A modlet that implements the "mostly dead" character death style for 7D2D.
For the manifesto, see: https://community.7daystodie.com/topic/19135-mostly-dead-a-manifesto/

This modlet deals only with the player death aspects.
The idea is that when a player character dies, players respawn as an entirely new character,
but they respawn into a (mostly) unchanged world.

## Features

When the player character dies:

* The player's inventory is deleted (the "DropOnDeath" setting is forced to "Delete All").
* Skill points are reset (not removed), and books that were read are forgotten.
* Crafting skill levels above 1 are randomly re-assigned to different crafting skills.
  _(New for A21)_
* Bedrolls and beds are no longer spawn points, so players do not spawn on them.
* The player map is reset, including any saved waypoints.
* Player vehicles are destroyed.
* The player's active land claim block is rendered inoperable.
* The player's ACL (Access Control List - i.e. friends list) is cleared.
* If the player hired any NPCs, those NPCs are dismissed. (Assumes SCore NPCs.)

The respawned character is at a similar "level" to the old one:

* If the player did not complete the "Basic Survival" quests, they start over from scratch.
  (This is so players can still earn the skill points from completing those quests.)
* If the player restarts before level 4, they will be given the "Whiteriver Citizen" quest
  so they can find a nearby trader.
* If restarting at level 3 or lower, the player will be given the player's starting items,
  _even if another mod or modlet has added starting items._
* When restarting at medium and higher levels,
  players are given _bundles_ of items so they have matching armor sets,
  and their given ammo matches their given weapons.
  This includes new bow and crossbow bundles (which have custom icons and translations).

## Technical Details

**This modlet contains custom assets and C# code.**
It is *not* compatible with EAC.
It must be installed on both clients and servers.

After installation, it is **highly** recommended to start a new game.

### Installation

Since A20, custom C# code is supported natively.
You do not have to compile anything.
Just drop the `MostlyDead` folder into the `Mods` folder,
like you would an XML/XPath modlet.

However, I still recommend using the Mod Launcher to install this modlet,
because its other features make it extremely useful.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/4068-the-7d2d-mod-launcher-a-mod-launcher-for-7-days-to-die/

### Customizing

Nearly all the features are added as triggered effects in the `buffResetPlayerOnDeath` buff.
To remove features you don't like, open this modlet's `buffs.xml` file.
Surround the tags representing the unwanted effects with XML comment tags (`<!-- -->`).

#### Permadeath

> :information_source: If you do not want to modify the XML yourself,
> I have created a modlet that will do it for you:
> [khzmusik_Mostly_Dead_Permadeath](../khzmusik_Mostly_Dead_Permadeath/)

"Permadeath" is where the player entirely starts over from the first day.
They are reset to level 1, do not have any skill points, and must do the starter quests again.

If this is the style of gameplay that you want,
there are instructions in the comments of the `buffs.xml` file.

To reset the player's crafting skill levels,
add an attribute called "reset_crafting" with a value of "true",
to the `triggered_effect` tag with the "ResetProgression" action:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetProgression" reset_books="true" reset_crafting="true" />
```

Since you are completely resetting the crafting skill levels,
there is no point in randomly re-assigning them first.
So, remove the the `triggered_effect` tag with the "RandomizeCraftingSkills" action:
```xml
<triggered_effect trigger="onSelfRespawn" action="RandomizeCraftingSkills, MostlyDead" target="self" />
```

Next, you will need to add this XML to the first `effect_group` node:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, MostlyDead" target="self" clear_difficulty_tiers="true" quests_on_reset="quest_StartNewCharacter,quest_BasicSurvival1" />
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerLevel, MostlyDead" target="self" />
<triggered_effect trigger="onSelfRespawn" action="AddBuff" buff="buffNewbieCoat" />
```

This XML is already in `buffs.xml` but it is commented out.

Here's what the XML does:
* Gives an immediately-completed quest that rewards the player with the appropriate starting items
  (this is done programmatically, so if other mods add additional starting items, they're included)
* Clears the quest difficulty tiers for the traders
* Gives the first "basic survival" quest
* Sets the player level back to 1 (and removes their XP in the process)
* Adds the "newbie coat" buff (since they are now "newbies" again)

The other `effect_group` nodes are now redundant and will conflict with the first one,
so you should comment those out (or remove them).

This can be customized even more, if you want:

* The `ResetPlayerQuests` action can take any quests you like, e.g. the "quest_StartNewCharacterT0"
  quest gives players their starting items *and* items they build doing the "basic survival" quests
* The `ResetPlayerLevel` action can reset to any level, not just level 1, and retain skill points:
  ```xml
  <triggered_effect trigger="onSelfRespawn" action="ResetPlayerLevel, MostlyDead" target="self" level="2" skill_points="5" />
  ```
* In `quests.xml` you can customize the "ItemsOnEnterGame" reward to exclude any items.
  This is done through a property named "exclude_items".
  For example, here is how to exclude the Duke note, food, water, and medical supplies:
  ```xml
  <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="1" stage="start">
      <property name="exclude_items" value="drinkJarBoiledWater,foodCanChili,medicalFirstAidBandage,noteDuke01" />
  </reward>
  ```

You should also comment out (or remove) this entry in `loadingscreen.xml`:
```xml
<tip key="loadingTipSkillsReset" />
```

That loading tip says explicitly that skill points are reset _but not removed,_
so it would no longer be accurate.

#### Limiting restarts to players level 2 and above

If you want this behavior, un-comment this line in the "buffResetPlayerOnDeath" buff:
```xml
<requirement name="PlayerLevel" operation="GT" value="1" />
```

That XML should be un-commented in these `effect_group` nodes:

* "Reset Player" (the main effect group that handles the level-agnostic reset effects)
* "Newbie" (handles low-level players who have _not_ finished the "Basic Survival" quests)
* "Tier 0" (handles low-level players who _have_ finished the "Basic Survival" quests)

#### Remembering books read

If you want players, after respawning, to remember the books they read,
set the `reset_books` attribute to "false" on the "ResetProgression" action in `buffs.xml`:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetProgression" reset_books="false"/>
```

#### Remembering crafting skill levels

If you want players, after respawning, to retain their crafting skill levels,
there are two approaches you can take.

If you want players to retain _all_ their skill levels (so none are re-assigned),
remove or comment out the "RandomizeCraftingSkills" action in `buffs.xml`:
```xml
<triggered_effect trigger="onSelfRespawn" action="RandomizeCraftingSkills, MostlyDead" target="self" />
```

If you want players to retain the levels of _particular crafting skills,_
you can protect them from randomization.
The levels of protected crafting skills will remain the same,
and will not be added to the "pool" of levels to randomly re-assign to other skills.

To do this, add the names of those skills as a comma-separated list in a "protected" attribute.
For example, this will protect workstation and vehicle crafting skills:
```xml
<triggered_effect trigger="onSelfRespawn" action="RandomizeCraftingSkills, MostlyDead" target="self" 
    protect="craftingWorkstations, craftingVehicles" />
```

#### Quest reset customization

The existing implementation resets the player's quests, then gives the player one of many
"StartNewCharacter" quests.
These quests are automatically completed, and reward the player with starting items.
The rewards are tiered, and _approximately_ scaled to the player level.

If you do not want this, you can remove the `effect_group` nodes that have the "PlayerLevel"
requirements in them.
If you still want to remove all the player quests, add a `triggered_effect` with the
"ResetPlayerQuests" action, to the first `effect_group` (the one without the "PlayerLevel"
requirement, where all the other effects are triggered).

These are the custom attributes used when resetting quests:

* `clear_difficulty_tiers` clears the quest difficulty tiers given by the trader,
  making the player start over at Tier I.
* `quests_on_reset` is a comma-separated list of quests to give the player when they respawn.

So if you want the "reset quests" feature to behave like the one in the ResetPlayerDMT modlet,
remove the `clear_difficulty_tiers` and `quests_on_reset` attributes from the relevant tag:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self"/>
```

If you want to send the player on a different quest after death,
set the `quests_on_reset` attribute value to the quest ID.
That attribute can accept a comma-separated list of quest ID, so you can specify more than one:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self" quests_on_reset="quest_StartNewCharacter,quest_BasicSurvival1"/>
```

If you don't want the player to start _any_ quests when they respawn,
you can set the `quests_on_reset` attribute value to either an empty string,
or to the special value "none":
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self" clear_difficulty_tiers="false" quests_on_reset="none"/>
```

#### Targeting other players

Many of the new actions accept a target value (in the `target` attribute).
This should almost always be the player ("self"), but it doesn't have to be.
Any of the other target values (like "other" or "selfAOE") will also work,
though only player characters are valid targets.

This could be used for some very weird things.
For example, imagine a club mod called the "ugly stick," used in PvP games.
When your power attack hits another player character with it, they immediately lose their ACL,
meaning they are no longer in the same party with their friends.

This is the effect you would add to that mod:
```xml
<triggered_effect trigger="onSelfSecondaryActionRayHit" action="ClearACL, Mods" target="other"/>
```

Feel free to experiment.

## Asset Credits

The assets in `UIAtlases/ItemIconAtlas` were created by myself.

However, they were modified from existing 7D2D assets,
making them _deriviative works_ of those assets.

Therefore, I do not claim any rights in those assets.

You may use them only under the same terms and conditions that The Fun Pimps grant for any other
7D2D assets.

## Thanks to KhaineGB

Most of the code in this modlet was inspired by KhaineGB and his ResetPlayerDMT modlet:
https://community.7daystodie.com/topic/19670-resetplayerdmt-a-hardcore-mode-for-7-days-to-die-a19/

Please thank him for the hard work and inspiration.
