﻿using System.Collections.Generic;
using System.Linq;

/// <summary>
/// <para>
/// Unclaims all the vehicles owned by the player. This means they are no longer owned by the
/// player (or anyone else) but they still exist in the world.
/// </para>
/// <para>
/// It turns out the differences between "unowned" and "player-owned" vehicles are negligible.
/// The player can still use the vehicle (even if locked); they can still pick it up;
/// it still shows on their HUD; etc. For this reason, it's not used in this mod;
/// you might as well do nothing to the player's vehicles.
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), for whom the active land claim block is removed.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// <example>
/// Example:
/// <code>
/// &lt;triggered_effect trigger="onSelfDied" action="RemovePlayerVehicles, Mods target="self" />
/// </code>
/// </example>
/// </summary>
public class MinEventActionUnclaimPlayerVehicles : MinEventActionTargetedBase
{
    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        var persistentPlayers = GameManager.Instance.GetPersistentPlayerList();

        var targetPlayers = new List<PlatformUserIdentifierAbs>();

        for (int i = 0; i < this.targets.Count; i++)
        {
            if (!(this.targets[i] is EntityPlayer player))
                continue;

            var playerData = persistentPlayers.GetPlayerDataFromEntityID(player.entityId);
            if (playerData == null)
                continue;

            targetPlayers.Add(playerData.PlatformUserIdentifier);
        }

        var entityVehicles = GameManager.Instance.World.Entities.list.OfType<EntityVehicle>();
        foreach (var entityVehicle in entityVehicles)
        {
            if (targetPlayers.Contains(entityVehicle.GetOwner()))
            {
                entityVehicle.SetOwner(null);
            }
        }
    }
}