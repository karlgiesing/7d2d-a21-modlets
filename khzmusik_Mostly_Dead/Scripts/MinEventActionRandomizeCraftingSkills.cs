﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

/// <summary>
/// <para>
/// Action to randomize the player's crafting skills.
/// Players will have the same total number of crafting levels, but they will be put
/// into random crafting skills.
/// </para>
/// <para>
/// If you are also using <see cref="MinEventActionResetProgression"/>,
/// you <em>should not</em> use that action's "reset_crafting" feature.
/// Conversely, if you <em>are</em> using that feature, then this action is useless.
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), whose crafting skills are randomized.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>protect</term>
///         <description>
///             Comma-separated list of the names of crafting skills to protect from randomization.
///             Their points will remain the same, and will not be included in the "pool" of
///             points to randomly re-assign to other crafting skills.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// <para>Examples:</para>
/// <example>
/// Randomize all crafting skills:
/// <code>
/// &lt;triggered_effect trigger="onSelfRespawn" action="RandomizeCraftingSkills, MostlyDead" target="self" />
/// </code>
/// </example>
/// <example>
/// Randomize all crafting skills <em>except</em> skills for workstations and vehicles:
/// <code>
/// &lt;triggered_effect trigger="onSelfRespawn" action="RandomizeCraftingSkills, MostlyDead" target="self" protect="craftingWorkstations, craftingVehicles" />
/// </code>
/// </example>
/// </summary>
public class MinEventActionRandomizeCraftingSkills : MinEventActionTargetedBase
{
    private static readonly Random random = new Random();

    private const string ATTR_PROTECT = "protect";

    /// <summary>
    /// List of the names of crafting skills that should be protected from randomization.
    /// Their points will remain the same, and will not be included in the "pool" of points to
    /// randomly re-assign to other crafting skills.
    /// </summary>
    public List<string> ProtectedSkills { get; private set; }

    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (!(targets[i] is EntityPlayer player))
                continue;

            RemoveRecipeBuffs(player);
            RandomizeCraftingSkills(player);
        }
    }

    /// <inheritdoc/>
    public override bool ParseXmlAttribute(XAttribute _attribute)
    {
        if (base.ParseXmlAttribute(_attribute))
            return true;

        if (ATTR_PROTECT.EqualsCaseInsensitive(_attribute.Name.LocalName))
        {
            
            ProtectedSkills = _attribute.Value.Split(',').Select(n => n.ToLower().Trim()).ToList();

            return true;
        }

        return false;
    }

    private void RemoveRecipeBuffs(EntityPlayer player)
    {
        var recipes = CraftingManager.GetRecipes();
        for (int i = 0; i < recipes.Count; i++)
        {
            if (recipes[i].IsLearnable)
            {
                player.Buffs.RemoveCustomVar(recipes[i].GetName());
            }
        }
    }

    private void RandomizeCraftingSkills(EntityPlayer player)
    {
        var progressionNames = new List<string>();
        var totalExtraLevels = 0;

        foreach (var progressionValue in player.Progression.GetDict().Values)
        {
            if (!progressionValue.ProgressionClass.IsCrafting)
                continue;

            var progressionName = progressionValue.Name.ToLower();

            if (ProtectedSkills?.Contains(progressionName) == true)
            {
                Log.Out($@"RandomizeCraftingSkills: {progressionName} is in ""{
                    string.Join(",", ProtectedSkills.ToArray())}"", ignoring");
                continue;
            }

            // Players always start with one level, not zero
            totalExtraLevels += progressionValue.Level - 1;

            progressionValue.Level = 1;
        }

        var count = progressionNames.Count;
        
        while (totalExtraLevels > 0 && count > 0)
        {
            var progressionName = progressionNames.ElementAt(random.Next(count));
            var progressionValue = player.Progression.GetProgressionValue(progressionName);

            int level = progressionValue.Level + 1;

            // If we're already at maximum, don't waste the level, and remove the name so we don't
            // try to add to it again.
            if (level > progressionValue.ProgressionClass.MaxLevel)
            {
                progressionNames.Remove(progressionName);
                count = progressionNames.Count;
                continue;
            }

            progressionValue.Level = level;
            totalExtraLevels--;
        }

        player.Progression.bProgressionStatsChanged = !player.isEntityRemote;
        player.bPlayerStatsChanged |= !player.isEntityRemote;
    }
}
