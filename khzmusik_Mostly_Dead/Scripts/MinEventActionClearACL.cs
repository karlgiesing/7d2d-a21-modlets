using Platform;
using System.Linq;

/// <summary>
/// <para>
/// Action to clear the player's Access Control List (ACL), which can be triggered by an event.
/// The ACL is the list of friends in multiplayer games, so removing it will "un-friend" the player
/// from all other players.
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), for whom the ACL is cleared.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// <example>
/// Example:
/// <code>
/// &lt;triggered_effect trigger="onSelfRespawn" action="ClearACL, Mods" target="self" />
/// </code>
/// </example>
/// </summary>
public class MinEventActionClearACL : MinEventActionTargetedBase
{
    /// <inheritdoc />
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] is EntityPlayer player)
                ClearACL(player);
        }
    }

    private static void ClearACL(EntityPlayer entityPlayer)
    {
        var persistentPlayers = GameManager.Instance.GetPersistentPlayerList();
        var playerData = persistentPlayers.GetPlayerDataFromEntityID(entityPlayer.entityId);

        if (playerData.ACL == null)
            return;

        foreach (var otherPlayerId in playerData.ACL.ToList())
        {
            GameManager.Instance.PersistentPlayerEvent(
                PlatformManager.InternalLocalUserIdentifier, //playerData.PlayerId,
                otherPlayerId,
                EnumPersistentPlayerDataReason.ACL_Removed);

            // I don't know if removing tracked entity IDs is actually necessary...
            // See if someone can test this in MP
            var otherData = persistentPlayers.GetPlayerData(otherPlayerId);
            entityPlayer.trackedFriendEntityIds.Remove(otherData.EntityId);

            var entityOther = GameManager.Instance.World.GetEntity(otherData.EntityId) as EntityPlayer;
            if (entityOther != null)
                entityOther.trackedFriendEntityIds.Remove(playerData.EntityId);
        }
    }
}