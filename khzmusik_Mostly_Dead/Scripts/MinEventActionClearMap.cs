using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// <para>
/// Action to clear the player's map and waypoints, which can be triggered by an event.
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), for whom the map is cleared.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// </summary>
/// <example>
/// Example:
/// <code>
/// &lt;triggered_effect trigger="onSelfRespawn" action="ClearMap, Mods" target="self" />
/// </code>
/// </example>
public class MinEventActionClearMap : MinEventActionTargetedBase
{
    private const BindingFlags _NonPublicFlags = BindingFlags.NonPublic | BindingFlags.Instance;

    /// <inheritdoc />
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            var player = targets[i] as EntityPlayer;
            if (player == null)
                continue;

            // This is now a public static method, in case something else wants to use it
            // (e.g. a "book" that clears the map, or something)
            ClearMap(player);
        }
    }

    /// <summary>
    /// Clears the map for the given player.
    /// </summary>
    /// <param name="player">The player entity.</param>
    public static void ClearMap(EntityPlayer player)
    {
        Log.Out("Clearing player map and waypoints...");

        if (player.ChunkObserver != null && player.ChunkObserver.mapDatabase != null)
            ClearMapChunkDatabase(player.ChunkObserver.mapDatabase);

        ClearWaypoints(player);

        Log.Out("Player map and waypoints cleared.");
    }

    private static void ClearMapChunkDatabase(MapChunkDatabase mapDatabase)
    {
        // The map chunk data is in private instance fields in the base type
        var type = typeof(MapChunkDatabase).BaseType;

        var catalog = (DictionaryKeyList<int, int>)type.GetField("catalog", _NonPublicFlags).GetValue(mapDatabase);
        if (catalog != null)
            catalog.Clear();

        var database = (DictionarySave<int, ushort[]>)type.GetField("database", _NonPublicFlags).GetValue(mapDatabase);
        if (database != null)
            database.Clear();

        var dirty = (Dictionary<int, bool>)type.GetField("dirty", _NonPublicFlags).GetValue(mapDatabase);
        if (dirty != null)
            dirty.Clear();
    }

    private static void ClearWaypoints(EntityPlayer player)
    {
        if (player.Waypoints != null && player.Waypoints.List != null)
        {
            foreach (var waypoint in player.Waypoints.List)
            {
                NavObjectManager.Instance.UnRegisterNavObject(waypoint.navObject);
            }
            player.Waypoints.List.Clear();
        }
        if (player.WaypointInvites != null)
            player.WaypointInvites.Clear();

        // Setting the marker position to zero clears the quick waypoint.
        // This won't work with remote players, but there's no better option.
        player.markerPosition = Vector3i.zero;
    }
}