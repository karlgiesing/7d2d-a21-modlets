﻿using System.Linq;

/// <summary>
/// This action dismisses any hired NPCs. Assumes SCore hiring implementation.
/// </summary>
public class MinEventActionDismissNPCs : MinEventActionTargetedBase
{
    /// <summary>
    /// SCore stores the hired NPC's entity ID as a cvar, with this prefix.
    /// The value of the cvar is the entity ID.
    /// </summary>
    public static readonly string BuffHiredPrefix = "hired_";

    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] is EntityPlayerLocal playerEntity)
            {
                DismissNPCs(playerEntity);
            }
        }
    }

    private void DismissNPCs(EntityPlayerLocal playerEntity)
    {
        var cvars = playerEntity.Buffs?.CVars;
        if (cvars == null)
            return;

        foreach (var cvar in cvars.Keys.ToList())
        {
            if (cvar.StartsWith(BuffHiredPrefix))
            {
                var entityId = (int)cvars[cvar];

                var npc = GameManager.Instance.World.GetEntity(entityId) as EntityAlive;
                if (npc == null)
                    continue;

                npc.Buffs.RemoveCustomVar("Leader");
                npc.Buffs.RemoveCustomVar("Owner");
                npc.Buffs.SetCustomVar("CurrentOrder", 3);
                playerEntity.Buffs.RemoveCustomVar(cvar);
            }
        }
    }
}
