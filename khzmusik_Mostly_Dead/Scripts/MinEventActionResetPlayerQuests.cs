using System.Xml.Linq;

/// <summary>
/// <para>
/// Resets the player's quests. By default, also resets the player's quest tiers, and adds a new
/// quest to find a trader after respawning (which also gives the player XP and skill points).
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), for whom quests are reset.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional (and discouraged).
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional (and discouraged).
///         </description>
///     </item>
///     <item>
///         <term>clear_difficulty_tiers</term>
///         <description>
///             Whether to clear the difficulty tiers for all traders, such that the player starts
///             over with Tier I quests.
///             Optional; defaults to true.
///         </description>
///     </item>
///     <item>
///         <term>quests_on_reset</term>
///         <description>
///             Quests to give to the player after all their current quests are reset.
///             It should be a comma-separated list of quest IDs (without spaces).
///             Setting it to an empty string, or to the special value "none", will mean no quests
///             are given after reset.
///             Optional; defaults to one quest, the "quest_PlayerRespawn" quest.
///         </description>
///     </item>
/// </list>
/// </para>
/// <para>
/// Original code by KhaineGB. Rewritten by khzmusik.
/// </para>
/// <example>
/// Examples:
/// <code>
/// &lt;!--
///     Resets the player's quests, resets the difficulty tiers of quests given by all traders,
///     and afterwards gives the player the "quest_PlayerRespawn" quest.
/// -->
/// &lt;triggered_effect trigger="onSelfBuffStart" action="ResetPlayerQuests, Mods" target="self"  /> 
///
/// &lt;!--
///     Resets the player's quests, but does not change the existing quest difficulty tiers,
///     and does not give any quests after reset.
///     (You could also use an empty string for the value of quests_on_reset.)
/// -->
/// &lt;triggered_effect trigger="onSelfBuffStart" action="ResetPlayerQuests, Mods" target="self" clear_difficulty_tiers="false" quests_on_reset="none" /> 
/// </code>
/// </example>
/// </summary>
public class MinEventActionResetPlayerQuests : MinEventActionTargetedBase
{
    private const string defaultQuestOnReset = "quest_PlayerRespawn";

    private bool clearQuestFactionPoints = true;

    private string[] questsOnReset = new string[] { defaultQuestOnReset };

    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            EntityPlayerLocal playerEntity = targets[i] as EntityPlayerLocal;

            if (playerEntity != null)
            {
                ClearActiveQuests(playerEntity);
                ClearQuestJournal(playerEntity);
                ClearQuestFactionPoints(playerEntity);
                AddQuests(playerEntity);
            }
        }
    }

    private static void ClearActiveQuests(EntityPlayerLocal player)
    {
        for (int i = player.QuestJournal.quests.Count - 1; i >= 0; i--)
        {
            if (player.QuestJournal.quests[i].CurrentState == Quest.QuestState.InProgress ||
                player.QuestJournal.quests[i].CurrentState == Quest.QuestState.ReadyForTurnIn)
            {
                Quest quest = player.QuestJournal.quests[i];

                player.QuestJournal.RemoveQuest(quest);
            }
        }
    }

    private static void ClearQuestJournal(EntityPlayerLocal player)
    {
        player.QuestJournal.quests.Clear();
        player.QuestJournal.sharedQuestEntries.Clear();
    }

    private void ClearQuestFactionPoints(EntityPlayerLocal player)
    {
        if (clearQuestFactionPoints)
        {
            player.QuestJournal.QuestFactionPoints.Clear();
        }
    }

    private void AddQuests(EntityPlayerLocal player)
    {
        if (questsOnReset == null || questsOnReset.Length == 0)
            return;

        foreach (string questId in questsOnReset)
        {
            if (string.IsNullOrEmpty(questId))
                continue;

            Quest myQuest = QuestClass.CreateQuest(questId);
            myQuest.QuestGiverID = -1;
            player.QuestJournal.AddQuest(myQuest);
        }
    }

    /// <inheritdoc/>
    public override bool ParseXmlAttribute(XAttribute _attribute)
    {
        if (base.ParseXmlAttribute(_attribute))
        {
            return true;
        }

        string name = _attribute.Name.LocalName;
        if (name == "clear_difficulty_tiers")
        {
            clearQuestFactionPoints = StringParsers.ParseBool(_attribute.Value);
            return true;
        }
        if (name == "quests_on_reset")
        {
            questsOnReset = ParseArray(_attribute.Value);
            return true;
        }

        return false;
    }

    private static string[] ParseArray(string str)
    {
        if (string.IsNullOrEmpty(str))
            return null;

        // "none" is a special value
        if (str.Trim().ToLower() == "none")
            return null;

        var subs = str.Split(',');
        for (int i = 0; i < subs.Length; i++)
            subs[i] = subs[i].Trim();
        return subs;
    }
}