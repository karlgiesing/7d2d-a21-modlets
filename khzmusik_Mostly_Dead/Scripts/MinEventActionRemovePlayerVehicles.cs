/// <summary>
/// <para>
/// Destroys all the vehicles owned by the player.
/// </para>
/// <para>
/// Original code by KhaineGB. Rewritten by khzmusik.
/// </para>
/// </summary>
/// <example>
/// <code>
/// &lt;triggered_effect trigger="onSelfDied" action="RemovePlayerVehicles, Mods"/>
/// </code>
/// </example>
public class MinEventActionRemovePlayerVehicles : MinEventActionBase
{
    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        //var steamId = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        var count = GameManager.Instance.World.Entities.list.Count;

        for (var i = 0; i < count; i++)
        {
            var entityVehicle = GameManager.Instance.World.Entities.list[i] as EntityVehicle;
            if (entityVehicle != null && entityVehicle.LocalPlayerIsOwner()) //.IsOwner(steamId))
            {
                Log.Out("Killing vehicle " + entityVehicle.EntityName);
                entityVehicle.Kill();
            }
        }
    }
}