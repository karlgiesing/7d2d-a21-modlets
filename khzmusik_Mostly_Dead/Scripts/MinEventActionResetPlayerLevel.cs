﻿using System.Xml.Linq;

/// <summary>
/// <para>
/// Action to reset the player's level, which will also remove skill points.
/// It may be customized to reset the player's level to something other than one,
/// and to specify the number of skill points that are retained after the player level is reset.
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), whose level(s) is (are) reset.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>level</term>
///         <description>
///             The level to which the player should be reset.
///             Must be a postive non-zero integer.
///             Optional; defaults to 1.
///         </description>
///     </item>
///     <item>
///         <term>skill_points</term>
///         <description>
///             The number of skill points which should be retained after the player level is reset.
///             Must be a postive integer, or zero.
///             Optional; defaults to 0.
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// <example>
/// Examples:
/// <code>
/// &lt;!-- Traditional "permadeath" play style; uses the defaults -->
/// &lt;triggered_effect trigger="onSelfRespawn" action="ResetPlayerLevel, MostlyDead" target="self" />
/// 
/// &lt;!-- Reset to just after the player finished the "basic survival" quests -->
/// &lt;triggered_effect trigger="onSelfRespawn" action="ResetPlayerLevel, MostlyDead" target="self" level="1" skill_points="4" />
/// </code>
/// </example>
/// </summary>
public class MinEventActionResetPlayerLevel : MinEventActionTargetedBase
{
    private int level = 1;
    private int skillPoints = 0;

    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] is EntityPlayerLocal player)
            {
                player.Progression.Level = level;
                player.Progression.SkillPoints = skillPoints;
                player.Progression.ExpToNextLevel = player.Progression.GetExpForNextLevel();
                // Don't give the player an experience point defecit since they're starting over
                player.Progression.ExpDeficit = 0;
            }
        }
    }

    /// <inheritdoc/>
    public override bool ParseXmlAttribute(XAttribute _attribute)
    {
        if (base.ParseXmlAttribute(_attribute))
            return true;

        int result;

        if ("level".EqualsCaseInsensitive(_attribute.Name.LocalName) &&
            int.TryParse(_attribute.Value, out result) &&
            result > 0)
        {
            level = result;
            return true;
        }

        if ("skill_points".EqualsCaseInsensitive(_attribute.Name.LocalName) &&
            int.TryParse(_attribute.Value, out result) &&
            result >= 0)
        {
            skillPoints = result;
            return true;
        }

        return false;
    }
}
