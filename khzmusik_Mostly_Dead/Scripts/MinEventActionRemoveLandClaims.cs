/// <summary>
/// <para>
/// Removes the active land claim block belonging to the target(s).
/// </para>
/// <para>
/// Supported tag attributes:
/// <list type="bullet">
///     <item>
///         <term>target</term>
///         <description>
///             The target entity (or entities), for whom the active land claim block is removed.
///             Only player entities are affected.
///             Valid target values: "self", "other", "selfAOE", "otherAOE", "positionAOE".
///             In nearly all cases you should use "self".
///             <strong>Required.</strong>
///         </description>
///     </item>
///     <item>
///         <term>range</term>
///         <description>
///             Maximum range (distance in meters) to include entities as targets.
///             Used only when the target is one of the AOE (Area Of Effect) values.
///             Optional.
///         </description>
///     </item>
///     <item>
///         <term>target_tags</term>
///         <description>
///             Adds entities with these tags, to the list of target entities.
///             Special tag values: "party", "ally", "enemy".
///             Optional.
///         </description>
///     </item>
/// </list>
/// </para>
/// <para>
/// Original code by KhaineGB. Rewritten by khzmusik.
/// </para>
/// <example>
/// Example:
/// <code>
/// &lt;triggered_effect trigger="onSelfDied" action="RemoveLandClaims, Mods" target="self" />
/// </code>
/// </example>
/// </summary>
public class MinEventActionRemoveLandClaims : MinEventActionTargetedBase
{
    /// <inheritdoc/>
    public override void Execute(MinEventParams _params)
    {
        var persistentPlayers = GameManager.Instance.GetPersistentPlayerList();
        for (int i = 0; i < this.targets.Count; i++)
        {
            var player = this.targets[i] as EntityPlayer;
            if (player == null)
                continue;

            var playerData = persistentPlayers.GetPlayerDataFromEntityID(player.entityId);
            if (playerData == null)
                continue;

            var lpBlocks = playerData.GetLandProtectionBlocks();
            for (var j = lpBlocks.Count - 1; j >= 0; j--)
            {
                var blockPos = lpBlocks[j];
                persistentPlayers.RemoveLandProtectionBlock(blockPos);
            }
        }
    }
}
