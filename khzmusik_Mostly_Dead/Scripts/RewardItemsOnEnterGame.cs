﻿using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// <para>
/// Rewards the player with whatever items they should get at the start of the game.
/// </para>
/// 
/// <para>
/// A custom property may be specified, called "exclude_items", that accepts a list of excluded
/// item names as its value.
/// </para>
/// <example>
/// Examples:
/// <code>
/// &lt;!-- Give the player everything they would start with -->
/// &lt;reward type="ResetPlayerLevel, MostlyDead" />
/// 
/// &lt;!-- Give the player everything except the note from the Duke and the torch -->
/// &lt;reward type="ResetPlayerLevel, MostlyDead" />
///     &lt;property name="exclude_items" value="noteDuke01,meleeToolTorch" />
/// &lt;/reward>
/// </code>
/// </example>
/// </summary>
public class RewardItemsOnEnterGame : BaseReward
{
    /// <summary>
    /// Property name used to exclude items they would normally get at the start of the game.
    /// </summary>
    public static string PropExcludeItems = "exclude_items";

    private static readonly FieldInfo itemsOnEnterGameInfo = typeof(EntityAlive).GetField(
        "itemsOnEnterGame",
        BindingFlags.Instance | BindingFlags.NonPublic);

    private List<int> excludeItemIds = null;

    /// <inheritdoc/>
    public override BaseReward Clone()
    {
        var reward = new RewardItemsOnEnterGame();
        CopyValues(reward);
        reward.excludeItemIds = new List<int>(excludeItemIds);
        return reward;
    }

    /// <inheritdoc/>
    public override void GiveReward(EntityPlayer player)
    {
        if (player is EntityPlayerLocal playerLocal &&
            itemsOnEnterGameInfo.GetValue(playerLocal) is List<ItemStack> itemsOnEnterGame &&
            itemsOnEnterGame?.Count > 0)
        {
            var playerInventory = LocalPlayerUI.GetUIForPlayer(playerLocal).xui.PlayerInventory;

            for (int i = 0; i < itemsOnEnterGame.Count; i++)
            {
                var itemStack = itemsOnEnterGame[i].Clone();

                if (excludeItemIds?.Count > 0 &&
                    excludeItemIds.Contains(itemStack.itemValue.GetItemId()))
                {
                    Log.Out("ItemsOnEnterGame: Excluding item " +
                        itemStack.itemValue.ItemClass.GetLocalizedItemName());
                    continue;
                }

                if (!playerInventory.AddItem(itemStack))
                {
                    playerInventory.DropItem(itemStack);
                }
            }
        }
    }

    /// <inheritdoc/>
    public override void ParseProperties(DynamicProperties properties)
    {
        base.ParseProperties(properties);

        if (properties.Values.ContainsKey(PropExcludeItems))
        {
            ParseExcludeItems(properties.Values[PropExcludeItems]);
        }
    }

    private void ParseExcludeItems(string excludeItems)
    {
        if (string.IsNullOrEmpty(excludeItems))
            return;

        excludeItemIds = new List<int>();

        var itemNames = excludeItems.Split(new char[] { ',' });

        foreach (string itemName in itemNames)
        {
            var itemId = ItemClass.GetItem(itemName.Trim(), true)?.GetItemId();

            if (itemId.HasValue)
                excludeItemIds.Add(itemId.Value);
        }

        // If all item names were invalid, set to null to avoid unnecessary checks
        if (excludeItemIds.Count < 1)
        {
            excludeItemIds = null;
        }
    }
}
