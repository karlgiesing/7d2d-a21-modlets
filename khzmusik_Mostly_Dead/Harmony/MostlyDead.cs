﻿using System.Reflection;

namespace Harmony
{
    /// <summary>
    /// This class implements IModAPI and allows Harmony to patch in all the custom classes.
    /// </summary>
    public class MostlyDead : IModApi
    {
        /// <inheritdoc/>
        public void InitMod(Mod _modInstance)
        {
            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}