# Remove NPC Core Characters

Removes NPC Core characters from **all** spawn groups.

This is mainly for people who want zombies that require NPC Core,
but don't want the human NPCs that spawn by default (Baker, Nurse, and Harley).

However, it may be useful for others as well.
If you have other NPC Packs installed, you might want to only use characters from those packs.

> :information_source: In NPC Core, there is a file called `entitygroups_nocorespawn.xml` in `Config/Options`.
> If you rename that file to `entitygroups.xml`, and replace the existing file with that name in `Config`,
> it will do exactly the same thing as this modlet.
> This modlet is for people who are not technically knowledgeable enough to do that,
> or just find it more convenient to install a modlet.

> :warning: This modlet will remove the Core NPCs from **all** spawn groups.
> This includes the entity groups that are used by NPC POI spawners.
> If you use any of those POIs, then _make sure_ the NPC Packs you use
> are set up to spawn into those entity groups.
> Otherwise, nothing will spawn in those POIs.

## Dependent and Compatible Modlets

Obviously, this modlet is dependent upon the `0-NPCCore` modlet.
That modlet, in turn, is dependent upon the `0-SCore` modlet.
Both modlets must be installed.

## Technical Details

This modlet uses XPath to modify the entity groups in `entitygroups.xml`.
It does not add any assets or use any C# code.
So, _this_ modlet is safe to install only on servers.

However, `0-XNPCCore` includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, that modlet should be installed on both servers and clients.

Additionally, the `0-SCore` modlet contains custom C# code.
So any modlets that depend on it _must_ be run **with EAC off.**
