# Trade Routes: Biome Progression

This mod changes the "Opening Trade Routes" quests,
so they progress through the biomes as the tier changes.

These are the biomes allowed per tier:

* Tier 2: Any biome except the Wasteland _(unchanged from vanilla)_
* Tier 3: Any biome except the Pine Forest or the Wasteland
* Tier 4: Any biome except the Pine Forest
* Tier 5: Snow or Wasteland biomes only
  (techincally, any biome except the Burnt Forest, Desert, or Pine Forest)

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.
