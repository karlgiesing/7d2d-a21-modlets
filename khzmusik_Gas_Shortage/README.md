# Gas Shortage

Makes gasoline scarce, and more difficult to craft.

## Features

* Gasoline cannot be harvested from vehicles.
    (It can still be _looted_ from vehicles.)
* Each oil shale yields one can of gas (not 10).
* Stacks of gasoline hold 1000 cans (not 5000).

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.

## Possible improvements

I thought it would be fun if players spawned with a 4x4 upon entering the game,
but without any gasoline,
so they would have to loot any cars and gas barrels they found in order to use it.
(I deliberately chose the 4x4 because it is a gas hog.)

I realize this is not a play style for everyone, so it is not enabled by default.

If you want to do this, un-comment the code in `entityclasses.xml`.
(It is the only code in the XML file, so it should be obvious.)

You could also have them spawn with a different vehicle if you like.
