# NPC Civilians

Adds "Civilian" human NPCs to the game.

All characters were created by khzmusik, using assets that are either free for re-use or CC-BY.
See the Technical Details section for more information and asset credits.

## Civilians

The pack contains these characters:

### Test Female
![Test Female](./UIAtlases/UIAtlas/npcTestFemaleKhz.png)

### Test Male
![Test Male](./UIAtlases/UIAtlas/npcTestMaleKhz.png)

### Female Aged
![Female Aged](./UIAtlases/UIAtlas/npcFemaleAgedKhz.png)

### Female Scrubs
![Female Scrubs](./UIAtlases/UIAtlas/npcFemaleScrubsKhz.png)

### Female Young 1
![Female Young 1](./UIAtlases/UIAtlas/npcFemaleYoung1Khz.png)

### Female Young 2
![Female Young 2](./UIAtlases/UIAtlas/npcFemaleYoung2Khz.png)

### Female Young 3
![Female Young 3](./UIAtlases/UIAtlas/npcFemaleYoung3Khz.png)

### Female Young 4
![Female Young 4](./UIAtlases/UIAtlas/npcFemaleYoung4Khz.png)

### Female Young 5
![Female Young 5](./UIAtlases/UIAtlas/npcFemaleYoung5Khz.png)

### Male Aged
![Male Aged](./UIAtlases/UIAtlas/npcMaleAgedKhz.png)

### Male Aged Scrubs
![Male Aged Scrubs](./UIAtlases/UIAtlas/npcMaleAgedScrubsKhz.png)

### Male Aged 2
![Male Aged 2](./UIAtlases/UIAtlas/npcMaleAged2Khz.png)

### Male Cowboy
![Male Cowboy](./UIAtlases/UIAtlas/npcMaleCowboyKhz.png)

### Male Young 1
![Male Young 1](./UIAtlases/UIAtlas/npcMaleYoung1Khz.png)

### Male Young 2
![Male Young 2](./UIAtlases/UIAtlas/npcMaleYoung2Khz.png)

### Male Young 3
![Male Young 3](./UIAtlases/UIAtlas/npcMaleYoung3Khz.png)

All of them are in the Whiteriver faction, and can be hired.

Every character can wield all supported NPC weapons.

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

So far as I am aware, it should work with any versions of those modlets.

## Technical Details

This modlet includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, this modlet should be installed on both servers and clients.

### Spawning

These NPCs are spawned into entity groups used by vanilla biome spawners.
NPCs are spawned with probabilities that should not overwhelm zombies.

Starting with version 1.2.0, the spawn group probabilities are calculated the same as the
probabilities from my `Progression` modlets for other authors' NPC packs.

Characters that are more likely to stay at home/work are not spawned into these entity groups:

* `SnowZombies`
* `ZombiesNight`
* `ZombiesForestDowntownNight`
* `ZombiesWastelandNight`

All characters are spawned into the entity groups used by NPC sleeper volumes:

* `npcWhiteriver*`
* `npcFriendly*`

### Bandit versions
_(New for A21)_

There are now bandit versions of these characters, for those who want them.
But I assumed most people did not want them, so they are not enabled by default.

In the `Config` folder of this pack, there are additional XML files:

* `entityclasses-bandits.xml`: contains bandit versions of the civilian entities
* `entitygroups-bandits.xml`: spawns the bandit versions into entity groups

There are different ways you can use them:

* If you want the bandit versions, and _not_ the civilian versions,
    rename the existing `entityclasses.xml` and `entitygroups.xml` files,
    then rename the others to `entityclasses.xml` and `entitygroups.xml`.
* If you want both, you'll have to copy the XML
    from the `entityclasses-bandits.xml` file to the `entityclasses.xml` file (of this pack),
    and from the `entitygropus-bandits.xml` file to the `entitygroups.xml` file.
    _Do not_ copy the `<config>` tags, just the XML between them.

The bandit versions spawn into the same biome entity groups as the Whiteriver civilians.
But they spawn into different entity groups used by NPC sleeper volumes:

* `npcBandits*`
* `npcEnemy*`

As per usual with my enemy entities, they can be "basic" or "advanced" versions.
The "basic" versions can't be interacted with or hired, but can be spawned into hordes.
The "advanced" versions can be interacted with or hired - provided your faction standing is good enough -
but cannot be spawned into hordes.

I assume most players want the "basic" enemies,
since they usually do not use mods that change faction relationships.
But if you don't, un-comment the XML at the bottom of the `entityclasses-bandits.xml` file.

### How the characters were created

The character models were created using one of two programs:

* Mixamo Fuse - a free program for making human-like characters.
    Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
    But it can still be used, and is still very good.
* [MakeHuman](http://www.makehumancommunity.org) -
    an open source program for making human characters.
    It includes many community assets.

Once the models were created, I exported them to `.obj` files, and rigged them in
[Mixamo](https://www.mixamo.com).
From Mixamo, I exported them to Unity `.fbx` files.

I then imported them into Unity to set up ragdolls, rigging, tags, material shaders, etc.

None of this would be possible were it not for the help of Xyth and Darkstardragon - thank you!

If you would like to know how to do all this yourself,
Xyth has an excellent series of tutorials on YouTube:

https://www.youtube.com/c/DavidTaylorCIO/playlists

### Re-use in other mods/modlets

Any rights that I hold in these characters, I hereby place into the public domain (CC0).
Feel free to re-use them in any of your mods or modlets.

However, I don't hold the rights to all the assets in the models.
I have used assets from Mixamo Fuse and MakeHuman, and do not hold any rights in those assets.
Those rights are retained by Fuse, and/or the members of the MakeHuman community.

This sounds worse than it is.
Both the Fuse and MakeHuman assets are released under very lenient licenses,
and it should not be any problem for mod authors to include them in their mods:

* Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
    though you cannot repackage and sell the assets.
* MakeHuman characters can include assets from the MakeHuman team, or community assets.
    The MakeHuman assets are CC0, and the community assets are usually CC0 or CC-BY.
    Distribution of characters that include CC-BY assets **must** abide by the terms of that license,
    and include appropriate credits for those assets.

Obviously, the characters also use assets from The Fun Pimps (sounds, animations, etc).
The Fun Pimps retain the rights in those assets.

### Asset Credits

These assets were used in the creation of the characters in this modlet.
The assets were created for, and used in, MakeHuman.

* **Double MH Braid 01**  
    author:  Elvaerwyn  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Eve Hill's Young_Hispanic_Female**  
    author: Eve Hill Sisters Production Company / 3d.sk / jimlsmith9999  
    license: license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **female half-boots**  
    author:  punkduck  
    license: license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **French Braid 01 Variation**  
    author:  Elvaerwyn  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **lace-choker**  
    author:  punkduck  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Necklace (Native American Fashion)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **Retro Top**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **skin male african middleage**  
    author:  Mindfront  
    license: [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) 
* **Skirt (Native American Fashion)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **String 4**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 
* **Tight Jeans (female)**  
    author:  punkduck  
    license: [CC-BY](https://creativecommons.org/licenses/by/2.0/) 

If I have missed any assets used in these characters, please let me know,
and I will credit the author or remove the character, as appropriate.

### How sleeper volume group probabilities are calculated for NPCs

The math to work out the probabilities is much too difficult to do by hand,
so I wrote a Node.js/JavaScript script that calculate them.
The script is in the `Scripts` directory.

Here is what the script does:

1. Calculate a score for all character _variations_ (character + weapon type),
    with a higher score being more difficult,
    according to:
    * NPC character health
    * NPC character damage resistance
    * Weapon damage over time
    
    The scores are later normalized, so all scores are within the range of 0 to 1.

2. Generate probabilities for each character _variation_ to spawn, for each gamestage tier.
    Mathematically, the script uses an equation for a line that goes through (0.5, 0.5).
    The _x_ value is the (normalized) character score,
    and the _y_ value is the probability (between 0 and 1) of that character spawning.

    Each gamestage tier uses a different slope for that line.
    Lower gamestages have a negative slope (lower score -> higher probability),
    and higher gamestages have a positive slope (lower score -> lower probability).

3. For each _character,_ scale the probabilities of its weapon variations,
    so they are between a low number (like 0.01) and the maximum probability of all variations.

    If a pack contains characters of very different "toughness" (say, one with 1000 health and
    another with 300 health), then each character's weapon variation probabilities tend to
    "bunch up" around each other.
    If this happens, then there won't be very much difference in probabilities between low and
    high gamestages.
    Scaling them in this way ensures that the character's different probabilities for weapon
    variations are more "spread out."

4. For each character, count its number of weapon variations,
    and divide all its probabilities by that count.

    This is so a character with many weapon variations will spawn at (roughly) the same
    probability as a character with fewer weapon variations.

5. Filter all the character weapon variations into ranged and melee.
    Doing this _after_ the probabilities are calculated,
    ensures consistency betwen the "all," "ranged," and "melee" groups
    for each gamestage.

6. Convert it all to XML, and write it to the console.

    You can write it to an XML file by simply redirecting the output to file.
    (You probably will need to modify it afterwards, e.g. to remove the existing NPCs.)

I left out a lot of the technical details (believe it or not),
but the script should be extensively commented.

The script also contains different functions for calculating the probabilities,
and the comments list out the advantages and disadvantages of each.

Feel free to read, modify, and use the scripts as you like.

### Using the scripts for your own NPCs

The easiest way to use the script is to copy it, rename it,
and move it into your own pack's `Scripts` folder.

If you are going to use this script to calculate the probabilities for your own NPCs,
then you should leave _most_ of the script as is.
Using different methods to calculate scores, probabilities, or scaling
will make your NPC pack unbalanced relative to others that use this script.

You should modify these functions:

* `buildCharacters`:
    * Remove all the existing characters _except the "baseline" character._
    * Add your own objects that represent your own characters.
        It is probably easiest to copy the "baseline" character and enter your own information.
* `buildFactions`:
    * Use the appropriate faction for your NPCs.
        The faction will be used to build the name of the entity group, so spell it correctly!
        Valid values are the ones in `npcs.xml` from `0-XNPCCore`.
* `buildNpcs`:
    * Substitute the appropriate faction into the code that reads `factions.XXX.All`,
        `factions.XXX.Ranged`, and `factions.XXX.Melee`
        (where `XXX` is whatever faction is in the file already).
    * Use `factions.friendly` or `factions.enemy` depending upon whether your NPCs are friendly
        (neutral or allies) or hostile (enemies).
