# No Crafting

Removes all crafting from the game, including cooking.

This mod was inspired by
[the Lucky Looter series](https://youtube.com/playlist?list=PLc594J9-WhOxPuElG9lPf8W9Emwr0P2uW)
from [Glock9](https://www.youtube.com/@Glock9).

## Features

* The only items the player can craft are the bedroll and land claim.
* Removed all Basics of Survival quests except for crafting a bedroll.
  (After completion, players still get the skill points and Whiteriver Citizen quest.)
* Most player-crafted workstations are removed from the game (destroyed versions are still in game).
  The exception is campfires, which cannot be crafted, but can be used for warmth if found in the wild.
* Schematics, workstations, skill magazines, and parts for assembling weapons, tools, and vehicles
  are removed from loot containers, trader stashes, quest rewards, and Twitch actions.
* Removed perk books that only give crafting recipes (e.g. Needle and Thread books);
  completion bonuses that only give recipes (e.g. stacks of ammo)
  now give experience buffs instead.
* Items or mods which could only be crafted (e.g. Fireman's Helmet) are added to
  loot containers and trader stashes.
* Crafting-related loading screen tips are removed.
* Localizations are updated to remove any mention of crafting, cooking, or forging
  (where possible, I'm sure I missed a lot).
* The window that displays the crafting skill magazines is removed from the UI.

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.

However, this mod is **not** save-game safe.
_You must start a new game after installing or removing this mod._

### Adding back recipes (like boiled water)

Some players would prefer to have the ability to boil water,
even if they cannot cook anything else.

Here is what you need to do to enable that:

* To allow campfires to boil water, go to the modlet's `recipes.xml` file,
  and remove the `<wildcard_forge_category />` tag from the XML for the boiled water recipe.
  That XML exists but is commented out. Un-comment it so it looks like this:
  ```xml
  <remove xpath="//recipe[@name='drinkJarBoiledWater']/wildcard_forge_category" />
  ```
* The recipe for boiled water requires a cooking pot, but those are removed from loot and traders.
  The easiest solution is to allow players to boil water without a cooking pot.
  That XML also exists but is commented out. Un-comment it so it looks like this:
  ```xml
  <removeattribute xpath="//recipe[@name='drinkJarBoiledWater']/@craft_tool" />
  ```
* To allow the player to craft campfires, go to the modlet's `recipes.xml` file,
  and remove the `<wildcard_forge_category />` tag from the XML for the campfire recipe.
  That XML exists but is commented out. Un-comment it so it looks like this:
  ```xml
  <remove xpath="//recipe[@name='campfire']/wildcard_forge_category" />
  ```
* If players are allowed to craft campfires,
  then you probably want them to do the "Build a Campfire" starting quest.
  You will need to change `quests.xml` so it goes to that quest rather than "Find a Trader."
  First, comment out the XML that chains the first quest to the "Find a Trader" quest:
  ```xml
  <!--
  <set xpath="//quest[@id='quest_BasicSurvival1']/reward[@type='Quest']/@id">quest_whiteRiverCitizen1</set>
  <insertAfter xpath="//quest[@id='quest_BasicSurvival1']/reward[last()]">
      <reward type="SkillPoints" value="4" chainreward="true" />
  </insertAfter>
  -->
  ```
  (You could also remove the XML entirely if you prefer.)

  Next, un-comment the XML that chains the first quest to the "Build a Campfire" quest:
  ```xml
  <set xpath="//quest[@id='quest_BasicSurvival1']/reward[@type='Quest']/@id">quest_BasicSurvival8</set>
  <insertAfter xpath="//quest[@id='quest_BasicSurvival8']/reward[last()]">
      <reward type="SkillPoints" value="4" chainreward="true" />
  </insertAfter>
  ```

If you want to enable other recipes, the process is basically the same.
You remove the `<wildcard_forge_category />` tag from the recipe so it is visible.

But depending upon the recipes, you might need to do more modifications to this modlet.
In particular, you probably will not want to remove certain recipes or skill books from traders or loot.
