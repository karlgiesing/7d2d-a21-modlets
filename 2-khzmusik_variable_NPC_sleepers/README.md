# Variable NPC Sleepers

This modlet changes the behavior of NPCs spawned into sleeper volumes in POIs (or other prefabs).

By default, NPCs are instantly awakened when they are spawned into sleeper volumes.
They are spawned in when the player is not a short distance away from the sleeper volume bounds
(I believe it's 9 blocks/meters away).

With these changes, NPCs behave differently according to the type of sleeper volume.

This modlet is also meant to be a tutorial for using the related feature flags from SCore.
If you are a mod author or POI designer who wants information on those flags,
then skip to the "Technical Details" section.

## Changes and Features

Here are the different types of sleeper volumes, how vanilla zombies behave when spawned into them,
and how NPCs now behave when spawned into them:

* **Passive** (this is very uncommon - used mostly when the entities are "decoration")
  * _Zombies:_ Do not wake up unless attacked.
  * _Human NPCs:_ Do not wake up unless attacked.
* **Active** (this is the most common)
  * _Zombies:_ Become "inert but listening" as soon as the player touches the sleeper volume.
    Wakes up if the player makes enough noise.
    May wake up if the player attacks another zombie in that sleeper volume,
    even if they do it from outside the bounds of the sleeper volume.
  * _Human NPCs:_ Fully wake up as soon as the player touches the sleeper volume,
    but is only aware of the player if the player is in their line of sight.
    May wake up if the player attacks another zombie in that sleeper volume,
    even if they do it from outside the bounds of the sleeper volume.
* **Attack** (less common, used mainly near "end loot" or in "jumpscare" situations)
  * _Zombies:_ Wake up, with the player as their target, as soon as the player touches the sleeper
    volume.
    May wake up if the player attacks another zombie in that sleeper volume,
    even if they do it from outside the bounds of the sleeper volume;
    if so, they are immediately aware of the player and target them.
  * _Human NPCs:_ Fully wake up as soon as they are spawned in, regardless of whether the player
    touched the sleeper volume at all.
    Becomes aware of the player as soon as they can see them.
    (This is the default behavior of all human NPCs without this modlet.)

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet.
That modlet, in turn, is dependent upon the `0-SCore` modlet.
Both modlets must be installed separately.

## Technical Details

This modlet uses XPath to modify XML files.

However, it depends upon the `0-XNPCCore` modlet.
That modlet, in turn, depends upon `0-SCore`.
Those modlets include new C# code and assets,
so they must be installed on both clients and servers,
and EAC **must** be turned off.

### Feature Flags Used

SCore provides two feature flags that determine how NPCs wake up in sleeper volumes.
An explanation if each is below.

#### `SleeperInstantAwake`

SCore provides a feature flag called "SleeperInstantAwake", which is a property of the entity
class as defined in `entityclasses.xml`.
Since it is a property of the entities themselves, it is either on or off per entity;
it can't be turned on or off in different sleeper volumes or POIs.

If true, then this entity will be instantly awoken when it is spawned into any sleeper volume
(regardless of the sleeper volume type).
It does not wait for the sleeper volume to wake it up.

If false or ommitted, then the entity will rely on the sleeper volume to wake it up.
This depends on the sleeper volume type (Passive, Active, or Attack).
See the "Changes and Features" section for an overview;
however, "Attack" volume behavior depends upon the "AttackVolumeInstantAwake" property (see below).

In NPCCore, this is set to "true" on the "npcMeleeTemplate" class,
from which all entities descend.

#### `AttackVolumeInstantAwake`

SCore provides a "config feature block" that holds configuration values and feature flags
for various features.

In that block's "AdvancedNPCFeatures" property, there is a property named
"AttackVolumeInstantAwake".
This flag determines how NPC entities behave when spawned into Attack sleeper volumes.
It does not affect non-NPC entities, and it does not affect other types of sleeper volumes.

If true, then this entity will be instantly awoken when it is spawned into an "Attack" volume.
This is the behavior of NPCs spawned into all sleeper volumes when "SleeperInstantAwake"
is set to "true", except it only happens for "Attack" volumes.

If false or ommitted, then the entity will rely on the sleeper volume to wake it up.
In most cases, this happens when the player touches the sleeper volume bounds,
though they can also be woken up by player attacks.

This is also the behavior of "Active" sleeper volumes, so if "SleeperInstantAwake" is "false",
and this property is "false" or omitted,
NPCs in "Active" and "Attack" sleeper volumes will behave the same way.
